﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Pix;
using Pix.Gameplay.Character;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gamecodeur
{
    public class Camera
    {
        public Vector2 Translation { get; private set; }
        public Vector2 Offset { get; private set; }

        Rectangle screen;
        public Matrix Transform { get; private set; }

        public Camera(Rectangle pScreen)
        {
            screen = pScreen;
        }

        public void Follow(Sprite target,Rectangle pZoneGame)
        {
            Matrix translation;
            Matrix offset;

            float translatX, offsetX;
            float translatY, offsetY;

            float difScreenX = (pZoneGame.Width / 2 - screen.Width / 2);
            float difScreenY = (pZoneGame.Height / 2 - screen.Height / 2);

            if (target.Position.X + target.BoundingBox.Width / 2 > pZoneGame.Width / 2 + difScreenX
                || pZoneGame.Width < screen.Width)
            {
                translatX = -pZoneGame.Width / 2;
                offsetX = screen.Width / 2 - difScreenX;
            }
            else
            {
                if (target.Position.X + target.BoundingBox.Width / 2 < screen.Width / 2)
                {
                    translatX = 0;
                    offsetX = 0;
                }
                else
                {
                    translatX = -target.Position.X - (target.BoundingBox.Width / 2);
                    offsetX = screen.Width / 2;
                }
            }


            if (target.Position.Y + target.BoundingBox.Height / 2 > pZoneGame.Height / 2
                + difScreenY
                || pZoneGame.Height < screen.Height)
            {
                translatY = -pZoneGame.Height / 2;
                offsetY = screen.Height / 2 - difScreenY;
            }
            else
            {
                if (target.Position.Y + target.BoundingBox.Height / 2 < screen.Height / 2)
                {
                    translatY = 0;
                    offsetY = 0;
                }
                else
                {
                    translatY = -target.Position.Y - (target.BoundingBox.Height / 2);
                    offsetY = screen.Height / 2;
                }
            }

            Translation = new Vector2(translatX, translatY);
            Offset = new Vector2(offsetX, offsetY);

            translation = Matrix.CreateTranslation(
            translatX,
            translatY,
            0);

            offset = Matrix.CreateTranslation(offsetX,offsetY, 0);

            Transform = translation * offset;
        }
    }
}
