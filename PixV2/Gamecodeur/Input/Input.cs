﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gamecodeur
{
    public enum MouseButtons
    {
        Left,
        Right,
        Middle
    }

    public class Input
    {
        static KeyboardState oldKBState;
        static KeyboardState newKBState;
        static MouseState oldMouseState;
        static MouseState newMouseState;
        static GamePadState oldGPState;
        static GamePadState newGPState;

        static public Point MousePosition
        {
            get
            {
                return newMouseState.Position;
            }
        }

        static public void GetState()
        {
            newKBState = Keyboard.GetState();
            newMouseState = Mouse.GetState();

            GamePadCapabilities capabilities = GamePad.GetCapabilities(PlayerIndex.One);

            if (capabilities.IsConnected)
            {
                newGPState = GamePad.GetState(PlayerIndex.One,
                    GamePadDeadZone.IndependentAxes);
            }
        }

        static public void ReinitializeState()
        {
            oldKBState = newKBState;
            oldMouseState = newMouseState;
            oldGPState = newGPState;
        }

        static public bool GetKeyUp(Keys pKey)
        {
            if (newKBState.IsKeyUp(pKey) && !oldKBState.IsKeyUp(pKey))
            {
                return true;
            }
            return false;
        }

        static public bool GetKeyDown(Keys pKey)
        {
            if (newKBState.IsKeyDown(pKey) && !oldKBState.IsKeyDown(pKey))
            {
                return true;
            }
            return false;
        }

        static public bool GetKey(Keys pKey)
        {
            if (newKBState.IsKeyDown(pKey) && oldKBState.IsKeyDown(pKey))
            {
                return true;
            }
            return false;
        }

        static public bool GetMouseButtonUp(MouseButtons pButtons)
        {
            switch (pButtons)
            {
                case MouseButtons.Left:
                    if (newMouseState.LeftButton == ButtonState.Released &&
                        oldMouseState.LeftButton != ButtonState.Released)
                    {
                        return true;
                    }
                    break;
                case MouseButtons.Right:
                    if (newMouseState.RightButton == ButtonState.Released &&
                        oldMouseState.RightButton != ButtonState.Released)
                    {
                        return true;
                    }
                    break;
                case MouseButtons.Middle:
                    if (newMouseState.MiddleButton == ButtonState.Released &&
                        oldMouseState.MiddleButton != ButtonState.Released)
                    {
                        return true;
                    }
                    break;
                default:
                    break;
            }
            return false;
        }

        static public bool GetMouseButtonDown(MouseButtons pButtons)
        {
            switch (pButtons)
            {
                case MouseButtons.Left:
                    if (newMouseState.LeftButton == ButtonState.Pressed &&
                        oldMouseState.LeftButton != ButtonState.Pressed)
                    {
                        return true;
                    }
                    break;
                case MouseButtons.Right:
                    if (newMouseState.RightButton == ButtonState.Pressed &&
                        oldMouseState.RightButton != ButtonState.Pressed)
                    {
                        return true;
                    }
                    break;
                case MouseButtons.Middle:
                    if (newMouseState.MiddleButton == ButtonState.Pressed &&
                        oldMouseState.MiddleButton != ButtonState.Pressed)
                    {
                        return true;
                    }
                    break;
                default:
                    break;
            }
            return false;
        }

        static public bool GetMouseButton(MouseButtons pButtons)
        {
            switch (pButtons)
            {
                case MouseButtons.Left:
                    if (newMouseState.LeftButton == ButtonState.Pressed &&
                        oldMouseState.LeftButton == ButtonState.Pressed)
                    {
                        return true;
                    }
                    break;
                case MouseButtons.Right:
                    if (newMouseState.RightButton == ButtonState.Pressed &&
                        oldMouseState.RightButton == ButtonState.Pressed)
                    {
                        return true;
                    }
                    break;
                case MouseButtons.Middle:
                    if (newMouseState.MiddleButton == ButtonState.Pressed &&
                        oldMouseState.MiddleButton == ButtonState.Pressed)
                    {
                        return true;
                    }
                    break;
                default:
                    break;
            }
            return false;
        }

        static public bool GetButtonUp(Buttons pButton)
        {
            if (newGPState.IsButtonUp(pButton) && !oldGPState.IsButtonUp(pButton))
            {
                return true;
            }
            return false;
        }

        static public bool GetButtonDown(Buttons pButton)
        {
            if (newGPState.IsButtonDown(pButton) && !oldGPState.IsButtonDown(pButton))
            {
                return true;
            }
            return false;
        }

        static public bool GetButton(Buttons pButton)
        {
            if (newGPState.IsButtonDown(pButton) && oldGPState.IsButtonDown(pButton))
            {
                return true;
            }
            return false;
        }
    }
}
