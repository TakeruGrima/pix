﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gamecodeur
{
    class Effect
    {
        public enum FadeType
        {
            OneWay,
            TwoWay
        }

        static public float Fading(float pFadeSpeed, float pAlpha,
            GameTime pGameTime,FadeType pFadeType,ref bool pIncrease)
        {
            if (pFadeType == FadeType.OneWay && pAlpha < 1f)
            {
                pAlpha += pFadeSpeed * (float)pGameTime.ElapsedGameTime.TotalSeconds;

                if (pAlpha > 1f)
                {
                    pAlpha = 1.0f;
                }
            }
            else if(pFadeType == FadeType.TwoWay)
            {
                if (!pIncrease)
                    pAlpha -= pFadeSpeed * (float)pGameTime.ElapsedGameTime.TotalSeconds;
                else
                    pAlpha += pFadeSpeed * (float)pGameTime.ElapsedGameTime.TotalSeconds;
                if (pAlpha < 0.0f)
                {
                    pIncrease = true;
                    pAlpha = 0.0f;
                }
                else if (pAlpha > 1.0f)
                {
                    pIncrease = false;
                    pAlpha = 1.0f;
                }
            }

            return pAlpha;
        }
    }
}
