﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Gamecodeur
{
    public class Sprite : IActor
    {
        private bool showBoundingBox = false;//for debug

        // IActor
        public bool CollisionActive { get; set; } = true;
        public Vector2 Position { get; set; }
        public Rectangle BoundingBox { get; private set; }
        public float vx;
        public float vy;
        public bool CollideActive { get; set; }

        // Sprite
        public Texture2D Texture { get; } = null;
        public bool ToRemove { get; set; }

        public int Width { get; protected set; }
        public int Height { get; protected set; }
        public string Name { get; set; }

        public Sprite(Texture2D pTexture)
        {
            Texture = pTexture;

            Width = Texture.Width;
            Height = Texture.Height;
        }

        public Sprite(int pWidth, int pHeight)
        {
            Width = pWidth;
            Height = pHeight;
        }

        public Sprite()
        {

        }

        public void Move(float pX,float pY)
        {
            Position = new Vector2(Position.X + pX, Position.Y + pY);
        }

        public virtual void TouchBy(IActor pBy)
        {
            
        }

        public void UpdateBoundingBox()
        {
            BoundingBox = new Rectangle(
                (int)Position.X,
                (int)Position.Y,
                Width,
                Height
                );
        }

        public virtual void Draw(MySpriteBatch pSpriteBatch)
        {
            if (showBoundingBox)
                pSpriteBatch.DrawRectangle(BoundingBox, 2, false, Color.Red,1);
            if(Texture!= null)
                pSpriteBatch.Draw(Texture, Position, Color.White);
        }

        public virtual void Update(GameTime pGameTime)
        {
            Move(vx, vy);

            UpdateBoundingBox();
        }
    }
}
