﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gamecodeur
{
    public class MySpriteBatch : SpriteBatch
    {
        public enum Direction
        {
            Horizontal,
            Vertical
        }

        Texture2D texturePoint;

        public MySpriteBatch(GraphicsDevice graphicsDevice) : base(graphicsDevice)
        {
            texturePoint = new Texture2D(GraphicsDevice, 1, 1);
        }

        public void DrawPoint(int x, int y, int scale, Color color, float opacity)
        {
            texturePoint.SetData(new[] { color });
            Draw(texturePoint, new Rectangle(x, y, 1 * scale, 1 * scale), color * opacity);

            texturePoint.SetData(new[] { Color.White });
        }

        public void DrawLine(int x, int y,Direction direction, int scale, int size, Color color)
        {
            DrawLine(new Vector2(x, y), direction, scale, size, color);
        }

        public void DrawLine(Vector2 pPosition, Direction direction, int scale, int size, Color color)
        {
            int x = (int)pPosition.X;
            int y = (int)pPosition.Y;
            texturePoint.SetData(new[] { color });
            if (direction == Direction.Horizontal)
            {
                Draw(texturePoint, new Rectangle(x,y, size * scale, 1 * scale), color);
            }
            else if (direction == Direction.Vertical)
            {
                Draw(texturePoint, new Rectangle(x,y, 1 * scale, size * scale), color);
            }

            texturePoint.SetData(new[] { Color.White });
        }

        public void DrawRectangle(int x, int y,int w,int h, int scale,bool fill, Color color,float pAlpha)
        {
            texturePoint.SetData(new[] { color });

            if (!fill)
            {
                int bw = 2; // Border width

                Draw(texturePoint, new Rectangle(x, y, bw, h), color * pAlpha); // Left
                Draw(texturePoint, new Rectangle(x + w, y, bw, h), color * pAlpha); // Right
                Draw(texturePoint, new Rectangle(x, y, w, bw), color * pAlpha); // Top
                Draw(texturePoint, new Rectangle(x, y + h, w+bw, bw), color * pAlpha); // Bottom
            }
            else
                Draw(texturePoint,new Rectangle(x,y, w * scale, h * scale), color * pAlpha);

            texturePoint.SetData(new[] { Color.White });
        }

        public void DrawRectangle(Vector2 pPosition, int w, int h, int scale, bool fill, Color color,float pAlpha)
        {
            DrawRectangle((int)pPosition.X, (int)pPosition.Y, w, h, scale, fill, color,pAlpha);
        }

        public void DrawRectangle(Rectangle pRectangle,int scale, bool fill, Color color,float pAlpha)
        {
            DrawRectangle(pRectangle.X,pRectangle.Y,pRectangle.Width,pRectangle.Height,scale, fill, color,pAlpha);
        }

        public void Draw(PrimitivTexture pPrimTexture,Vector2 pPosition,Color pBackColor,float pAlpha,bool pFlip)
        {
            for (int j = 0; j < pPrimTexture.Height; j++)
            {
                if (pFlip == false)
                {
                    for (int i = 0; i < pPrimTexture.Width; i++)
                    {
                        if (pPrimTexture.Graph[j, i] == '1')
                        {
                            DrawPoint((int)pPosition.X + i * 2, (int)pPosition.Y + j * 2, 2, Color.White, pAlpha);
                        }
                        else if (pPrimTexture.Graph[j, i] == '2')
                        {
                            DrawPoint((int)pPosition.X + i * 2, (int)pPosition.Y + j * 2, 2, pBackColor, 1);
                        }
                    }
                }
                else
                {
                    for (int i = pPrimTexture.Width - 1; i >= 0; i--)
                    {
                        if (pPrimTexture.Graph[j, i] == '1')
                        {
                            DrawPoint((int)pPosition.X + ((pPrimTexture.Width - 1) * 2 - i * 2),(int)pPosition.Y + j * 2, 
                                2, Color.White, pAlpha);
                        }
                        else if (pPrimTexture.Graph[j, i] == '2')
                        {
                            DrawPoint((int)pPosition.X + ((pPrimTexture.Width - 1) * 2 - i * 2),(int)pPosition.Y + j * 2, 
                                2, pBackColor, 1);
                        }
                    }
                }
            }
        }
    }
}
