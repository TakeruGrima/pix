﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Gamecodeur
{
    public class GUIText : GUIPanel
    {
        public enum TextEffect { TYPING, NONE}

        public TextEffect Effect { get; set; } = TextEffect.NONE;

        public string Text { get; set; }
        public Color Color { get; set; }

        SpriteFont font;

        public int TextW { get; private set; }
        public int TextH { get; private set; }


        //Variables use for the Typing effect
        public float Time = 0;
        public float SpeedTyping = 1;

        private string displayText = string.Empty;
        private int currentIndex = 0;

        public bool Display = false;

        public GUIText(SpriteFont pFont, string pText,
            int pPanelW, int pPanelH) : this(pFont,pText,Color.White,pPanelW,pPanelH)
        {
            UpdateBoundingBox();
        }

        public GUIText(SpriteFont pFont, string pText, Color pColor,
            int pPanelW, int pPanelH) : base(pPanelW, pPanelH)
        {
            font = pFont;
            Color = pColor;
            Text = pText;

            TextW = MeasureTextW();
            TextH = MeasureTextH();

            if (pPanelW == 0)
                Width = TextW;
            if (pPanelH == 0)
                Height = TextH;

            UpdateBoundingBox();
        }

        public GUIText(SpriteFont pFont, string pText,Color pColor)
        {
            font = pFont;
            Color = pColor;
            Text = pText;

            TextW = MeasureTextW();
            TextH = MeasureTextH();

            Width = TextW;
            Height = TextH;

            UpdateBoundingBox();
        }

        public GUIText(SpriteFont pFont, string pText):
            this(pFont,pText,Color.White)
        {
            UpdateBoundingBox();
        }

        public void SetEffect(TextEffect pEffect)
        {
            Effect = pEffect;
        }

        public void ResetText()
        {
            displayText = string.Empty;
            currentIndex = 0;
        }

        public int MeasureTextW()
        {
            return (int)font.MeasureString(Text).X;
        }

        public int MeasureTextH()
        {
            return (int)font.MeasureString(Text).Y;
        }

        public void ParseText()
        {
            String line = String.Empty;
            String returnString = String.Empty;
            String[] wordArray = Text.Split(' ');

            foreach (String word in wordArray)
            {
                if (font.MeasureString(line + word).Length() > parent.Width)
                {
                    returnString = returnString + line + '\n';
                    line = String.Empty;
                }

                line = line + word + ' ';
            }

            Text = returnString + line;
        }

        public override void Update(GameTime pGameTime)
        {
            base.Update(pGameTime);

            int x = (int)LocalPosition.X;
            int y = (int)LocalPosition.Y;

            if (parent != null)
            {
                x += (int)parent.Position.X;
                y += (int)parent.Position.Y;
            }

            switch (hAlign)
            {
                case Align.Center:
                    x += (Width - TextW) / 2;
                    break;
            }

            switch (vAlign)
            {
                case Align.Center:
                    y += (Height - TextH) / 2;
                    break;
            }

            Position = new Vector2(x, y);

            Time += (float)pGameTime.ElapsedGameTime.TotalSeconds;

            if (Effect == TextEffect.TYPING && 
                Time >= (60 * (float)pGameTime.ElapsedGameTime.TotalSeconds) / SpeedTyping && currentIndex < Text.Length)
            {
                displayText += Text[currentIndex];
                currentIndex++;

                Time = 0;
            }
            else
            {
                Display = true;
            }

            UpdateBoundingBox();
        }

        public override void Draw(MySpriteBatch pSpriteBatch)
        {
            if (Visible)
            {
                switch (Effect)
                {
                    case TextEffect.TYPING:
                        pSpriteBatch.DrawString(font, displayText, Position, Color * Alpha);
                        break;
                    case TextEffect.NONE:
                        pSpriteBatch.DrawString(font, Text, Position, Color * Alpha);
                        break;
                    default:
                        break;
                }
            }
        }
    }
}
