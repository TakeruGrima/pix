﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Graphics;

namespace Gamecodeur
{
    public class GUIDialogueBox : GUIPanel
    {
        public GUIText TextGUI { get; set; }

        private int nbLine = 2;
        private int ecard = 10;
        private float normalSpeedTyping = 20f;

        private string FullText { get; set; }

        private int currentIndex = 0;

        public string Text
        {
            get
            {
                return TextGUI.Text;
            }
            set
            {
                TextGUI.ResetText();
                TextGUI.Text = value;
            }
        }

        public float SpeedTyping
        {
            get
            {
                return TextGUI.SpeedTyping;
            }
            set
            {
                TextGUI.SpeedTyping = value;
            }
        }

        public GUIText.TextEffect Effect
        {
            get
            {
                return TextGUI.Effect;
            }
            set
            {
                TextGUI.Effect = value;
            }
        }

        public GUIDialogueBox(SpriteFont pFont, string pText, Texture2D pTexture) : base(pTexture)
        {
            CreateText(pFont, pText, Color.White);
        }

        public GUIDialogueBox(SpriteFont pFont, string pText, int pWidth, int pHeight) : base(pWidth, pHeight)
        {
            CreateText(pFont, pText, Color.White);
        }

        public GUIDialogueBox(SpriteFont pFont, string pText, int pWidth, int pHeight, Color pColor) : base(pWidth, pHeight, pColor)
        {
            CreateText(pFont, pText, Color.White);
        }

        public GUIDialogueBox(SpriteFont pFont, string pText, int pWidth, int pHeight, Color pColor, Color pTextColor) : base(pWidth, pHeight, pColor)
        {
            CreateText(pFont, pText, pTextColor);
        }

        public GUIDialogueBox()
        {

        }

        private void CreateText(SpriteFont pFont, string pText, Color pColor)
        {
            TextGUI = new GUIText(pFont, pText)
            {
                LocalPosition = new Vector2(ecard, ecard),
                hAlign = Align.Center,
                vAlign = Align.Center,
                Color = pColor
            };
            AddElement(TextGUI);

            TextGUI.ParseText();

            FullText = TextGUI.Text;

            WrapText(TextGUI.Text);

            Height = TextGUI.BoundingBox.Height;

            Width += ecard*2;
            Height += ecard*2;

            TextGUI.UpdateBoundingBox();
        }

        private void WrapText(string pText)
        {
            int i = 0;
            int index = 0;

            string subString = pText;

            while (i < nbLine)
            {
                if (pText.Contains('\n'))
                {
                    index += subString.IndexOf('\n') + 1;
                    subString = subString.Substring(subString.IndexOf('\n') + 1);
                    i++;
                }
                else
                {
                    index = pText.Length;
                    i++;
                }
            }

            TextGUI.Text = pText.Substring(0, index);
        }

        public override void TouchBy(IActor pBy)
        {
            base.TouchBy(pBy);
        }

        public override void Update(GameTime pGameTime)
        {
            switch (Effect)
            {
                case GUIText.TextEffect.TYPING:
                    if (Input.GetKey(Keys.B))
                    {
                        SpeedTyping = normalSpeedTyping * 10;
                    }
                    else
                    {
                        SpeedTyping = normalSpeedTyping;
                    }
                    break;
                case GUIText.TextEffect.NONE:
                    break;
                default:
                    break;
            }

            if(TextGUI.Display && Input.GetKeyDown(Keys.A))
            {
                currentIndex += Text.Length;
                if (FullText.Length  > currentIndex)
                {
                    WrapText(FullText.Substring(currentIndex));
                    TextGUI.ResetText();
                }
            }
            base.Update(pGameTime);
        }

        public override void Draw(MySpriteBatch pSpriteBatch)
        {
            base.Draw(pSpriteBatch);
        }
    }
}
