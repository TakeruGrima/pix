﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gamecodeur
{
    public class GUIPanel : Sprite
    {
        public enum Align
        {
            Center,
            Bottom,
            Top,
            Left,
            Right
        }

        //public field
        public bool Visible { get; set; } = true;
        public Vector2 LocalPosition { get; set; }

        public Align hAlign = Align.Left;
        public Align vAlign = Align.Top;

        public float Alpha { get; set; } = 1;

        //private field
        public bool noColor = false;
        public Color color = Color.White;
        protected List<GUIPanel> elements;
        protected GUIPanel parent;

        public GUIPanel(Texture2D pTexture) : base(pTexture)
        {
            parent = this;
            elements = new List<GUIPanel>();
        }

        public GUIPanel(int pWidth, int pHeight) : base(pWidth, pHeight)
        {
            parent = this;
            elements = new List<GUIPanel>();
            noColor = true;
        }

        public GUIPanel(int pWidth, int pHeight, Color pColor) : base(pWidth, pHeight)
        {
            parent = this;
            color = pColor;
            elements = new List<GUIPanel>();
            noColor = false;
        }

        public GUIPanel()
        {
            parent = this;
            elements = new List<GUIPanel>();
            noColor = true;
        }

        public void SetParent(GUIPanel pParent)
        {
            parent = pParent;
        }

        public void AddElement(GUIPanel pElement)
        {
            pElement.SetParent(this);
            elements.Add(pElement);
        }

        public GUIPanel Find(string pName)
        {
            foreach (GUIPanel element in elements)
            {
                if (element.Name == pName)
                    return element;
            }

            return null;
        }

        public GUIText FindGuiText(string pText)
        {
            foreach (GUIPanel element in elements)
            {
                if(element is GUIText text)
                {
                    if(text.Text == pText)
                    {
                        return text;
                    }
                }
            }
            return null;
        }

        public override void Update(GameTime pGameTime)
        {
            int x = (int)LocalPosition.X;
            int y = (int)LocalPosition.Y;

            if (parent != null && parent!=this)
            {
                x += (int)parent.Position.X;
                y += (int)parent.Position.Y;
            }

            switch (hAlign)
            {
                case Align.Center:
                    x += (parent.Width - Width) / 2;
                    break;
                case Align.Right:
                    x += parent.Width - Width;
                    break;
            }

            switch (vAlign)
            {
                case Align.Center:
                    y += (parent.Height - Height) / 2;
                    break;
                case Align.Bottom:
                    y += parent.Height -Height;
                    break;
            }

            Position = new Vector2(x, y);

            base.Update(pGameTime);

            foreach (GUIPanel element in elements)
            {
                element.Update(pGameTime);
            }

            UpdateBoundingBox();
        }

        public override void Draw(MySpriteBatch pSpriteBatch)
        {
            if (Visible)
            {
                if (Texture == null && noColor == false)
                {
                    if (color != null)
                    {
                        pSpriteBatch.DrawRectangle(
                            Position, BoundingBox.Width, BoundingBox.Height, 1, false, color,Alpha);
                    }
                    else
                    {
                        pSpriteBatch.DrawRectangle(
                            Position, BoundingBox.Width, BoundingBox.Height, 1, false, Color.White,Alpha);
                    }
                }
                else
                    base.Draw(pSpriteBatch);

                foreach (GUIPanel element in elements)
                {
                    element.Draw(pSpriteBatch);
                }
            }
        }
    }
}
