﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System.Diagnostics;

namespace Gamecodeur
{
    public delegate void OnClick(GUIButton pSender);

    public class GUIButton : GUIPanel
    {
        public string Text
        {
            get
            {
                return TextGui.Text;
            }
            set
            {
                TextGui.Text = value;
            }
        }

        public GUIText TextGui { get; private set; }

        public bool clickable = true;

        public bool IsHover { get; private set; }
        public OnClick OnClick { get; set; }

        public bool FitText = false;

        public GUIButton()
        {
        }

        public GUIButton(Texture2D pTexture) : base(pTexture)
        {
        }

        public GUIButton(SpriteFont pFont, string pText,Texture2D pTexture) : base(pTexture)
        {
            CreateText(pFont, pText, Color.White);
        }

        public GUIButton(SpriteFont pFont,string pText, int pWidth, int pHeight) : base(pWidth, pHeight)
        {
            CreateText(pFont, pText, Color.White);
        }

        public GUIButton(SpriteFont pFont, string pText, int pWidth, int pHeight, Color pColor) : base(pWidth, pHeight, pColor)
        {
            CreateText(pFont, pText,Color.White);
        }

        private void CreateText(SpriteFont pFont, string pText,Color pColor)
        {
            TextGui = new GUIText(pFont, pText)
            {
                LocalPosition = new Vector2(0, 0),
                hAlign = Align.Center,
                vAlign = Align.Center,
                Color = pColor
            };
            AddElement(TextGui);

            TextGui.UpdateBoundingBox();

            if (Width == 0)
                Width = TextGui.BoundingBox.Width;
            if (Height == 0)
                Height = TextGui.BoundingBox.Height;
        }

        public override void Update(GameTime pGameTime)
        {
            TextGui.Alpha = Alpha;
            if (FitText)
            {
                Width = TextGui.TextW;
                Height = TextGui.TextH;
            }
            base.Update(pGameTime);

            UpdateBoundingBox();

            if(clickable)
            {
                Point MousePos = Input.MousePosition;

                if (BoundingBox.Contains(MousePos))
                {
                    if (!IsHover)
                    {
                        IsHover = true;
                        Debug.WriteLine("The button is now hover");
                    }
                }
                else
                {
                    if (IsHover == true)
                    {
                        Debug.WriteLine("The button is now more hover");
                    }
                    IsHover = false;
                }

                if (IsHover)
                {
                    if (Input.GetMouseButtonDown(MouseButtons.Left))
                    {
                        Debug.WriteLine("Button is clicked");
                        OnClick?.Invoke(this);
                    }
                }
            }
        }

        public override void Draw(MySpriteBatch pSpriteBatch)
        {
            if(Visible)
            {
                base.Draw(pSpriteBatch);

                TextGui.Draw(pSpriteBatch);
            }
        }
    }
}
