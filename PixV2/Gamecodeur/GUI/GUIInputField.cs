﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Gamecodeur
{
    public class GUIInputField : GUIPanel
    {
        public GUIInputField(Texture2D pTexture) : base(pTexture)
        {
        }

        public GUIInputField(int pWidth, int pHeight) : base(pWidth, pHeight)
        {
        }

        public GUIInputField(int pWidth, int pHeight, Color pColor) : base(pWidth, pHeight, pColor)
        {
        }

        public GUIInputField()
        {
        }

        public override void TouchBy(IActor pBy)
        {
            base.TouchBy(pBy);
        }

        public override void Draw(MySpriteBatch pSpriteBatch)
        {
            base.Draw(pSpriteBatch);
        }

        public override void Update(GameTime pGameTime)
        {
            base.Update(pGameTime);
        }
    }
}
