﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace Gamecodeur
{
    public class GUINavigation : GUIPanel
    {
        public int OldFocus { get; private set; } = 0;
        public int Focus { get; private set; } = 0;

        public GUIPanel FocusPanel
        {
            get
            {
                return elements[Focus];
            }
        }

        public GUINavigation(int pWidth, int pHeight) : base(pWidth, pHeight)
        {

        }

        public override void Update(GameTime pGameTime)
        {
            base.Update(pGameTime);

            UpdateBoundingBox();

            if (elements.Count == 0)
                return;

            if (Input.GetKeyDown(Keys.Up))
            {
                OldFocus = Focus;
                if (Focus > 0)
                    Focus--;
                else if (Focus == 0)
                    Focus = elements.Count - 1;
                Debug.WriteLine("Oldfocus :" + OldFocus);
                Debug.WriteLine("focus :" + Focus);
            }
            else if (Input.GetKeyDown(Keys.Down))
            {
                OldFocus = Focus;
                if (Focus < elements.Count - 1)
                    Focus++;
                else if (Focus == elements.Count - 1)
                    Focus = 0;
                Debug.WriteLine("Oldfocus :" + OldFocus);
                Debug.WriteLine("focus :" + Focus);
            }

            if (Input.GetKeyDown(Keys.Enter))
            {
                GUIButton button = FocusPanel as GUIButton;
                button.OnClick?.Invoke(button);
            }
        }

        public override void Draw(MySpriteBatch pSpriteBatch)
        {
            base.Draw(pSpriteBatch);
        }

        public void ApplyFadingEffect(GameTime pGameTime, float pFadeSpeed,ref bool increase)
        {
            if (OldFocus != Focus)
            {
                foreach (GUIPanel element in elements)
                {
                    if(FocusPanel!=element)
                    {
                        if (element.Alpha < 1)
                        {
                            element.Alpha = Gamecodeur.Effect.Fading(pFadeSpeed, element.Alpha, pGameTime, Gamecodeur.Effect.FadeType.OneWay, ref increase);
                        }
                        else
                            element.Alpha = 1;
                    }
                }
            }
            FocusPanel.Alpha = Gamecodeur.Effect.Fading(pFadeSpeed, FocusPanel.Alpha, pGameTime, Gamecodeur.Effect.FadeType.TwoWay, ref increase);
        }
    }
}
