﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gamecodeur
{
    public static class ExtensionMethods
    {
        public static string GetSubstring(this String str,string substring)
        {
            int indexStart = str.IndexOf(substring);
            int lenght = substring.Length;

            return str.Substring(indexStart, lenght);
        }
    }
}
