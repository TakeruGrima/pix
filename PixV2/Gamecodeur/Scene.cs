﻿using Microsoft.Xna.Framework;
using Pix;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gamecodeur
{
    /// <summary>
    /// This class is the base class of all Scene in the Game
    /// It manages Load, Unload, Draw, Update of all actors in a scene
    /// </summary>
    abstract public class Scene
    {
        /// <summary>
        /// The mainGame class who manages the Load of Ressources, Unload,Update
        /// content and Draw.
        /// </summary>
        protected MainGame mainGame;

        /// <summary>
        /// The camera use to follow a sprite.
        /// </summary>
        protected Camera camera = null;

        /// <summary>
        /// The listActors is a list of <see cref="IActor"/> who contains all 
        /// drawable content, all will be drawn in this class.
        /// </summary>
        protected List<IActor> listActors;

        /// <summary>
        /// The GUI is the main Panel who contains all your GUI component
        /// such as GUIText,GUIButton ...
        /// </summary>
        protected GUIPanel GUI;

        /// <summary>
        /// The Screen who contains the resolution of the game.
        /// </summary>
        protected Rectangle Screen;

        /// <summary>
        /// The firstUpdate boolean is true where the first update is done.
        /// </summary>
        protected bool firstUpdate = false;

        /// <summary>
        /// Initializes a new instance of the <see cref="Scene"/> class.
        /// </summary>
        /// <param name="pGame">The p game is the MainGame.</param>
        /// <remarks>This constructor initialize <see cref="mainGame"/>
        /// and <see cref="listActors"/></remarks>.
        public Scene(MainGame pGame)
        {
            mainGame = pGame;
            listActors = new List<IActor>();
        }

        /// <summary>
        /// Clear the <see cref="listActors"/>.
        /// </summary>
        public void Clean()
        {
            listActors.RemoveAll(item => item.ToRemove == true);
        }

        /// <summary>
        /// Called when [GUI] initialize the first <see cref="GUIPanel"/>.
        /// </summary>
        /// <remarks>
        /// This method is made to be override by the child class to add
        /// some GUI element, see the example bellow.
        /// </remarks>
        /// <example>
        /// <code> 
        ///     public override void OnGUI()
        ///     {
        ///         base.OnGUI();
        ///
        ///         GUIText title = new GUIText(
        ///             mainGame.Content.Load&lt;SpriteFont&gt;("Fonts/Title"),
        ///             "PIX", Screen.Width, Screen.Height / 2)
        ///         {
        ///             Name = "Title",
        ///             LocalPosition = new Vector2(0, 0),
        ///             hAlign = GUIPanel.Align.Center,
        ///             vAlign = GUIPanel.Align.Center
        ///         };
        ///
        ///         GUI.AddElement(title);
        ///     }
        /// </code>
        /// </example>
        /// <seealso cref="GUIText"/>
        /// <seealso cref="GUIButton"/>
        /// <seealso cref="GUINavigation"/>
        /// <seealso cref="GUIDialogueBox"/>
        /// <seealso cref="Microsoft.Xna.Framework.Graphics.SpriteFont"/>
        public virtual void OnGUI()
        {
            Screen = mainGame.Window.ClientBounds;

            GUI = new GUIPanel(Screen.Width, Screen.Height)
            {
                Position = new Vector2(0, 0),
            };

            GUI.SetParent(new GUIPanel(Screen.Width, Screen.Height));
        }

        /// <summary>
        /// Loads all content for the scene.
        /// via <see cref="OnGUI"/>
        /// </summary>
        /// <remarks>
        /// Needs to be overrided by child class to load their own content, 
        /// initialize also GUI content
        /// </remarks>
        /// <example>
        /// <code> 
        ///     public override void Load()
        ///     {
        ///         base.Load();
        ///
        ///         Sprite sprite = new Sprite(mainGame.Content.Load&lt;Texture2D&gt;("sprite1"));
        ///         listActors.Add(sprite);
        ///     }
        /// </code>
        /// </example>
        public virtual void Load()
        {
            OnGUI();
        }

        /// <summary>
        /// Unload all actor you create for your scene
        /// </summary>
        public virtual void UnLoad()
        {
            foreach (IActor actor in listActors)
            {
                actor.ToRemove = true;
            }

            Clean();
        }

        /// <summary>
        /// Updates all actors <see cref="listActors"/>( usefull for movement and other change)
        /// </summary>
        /// <param name="gameTime">The game time.</param>
        /// <remarks>
        /// Make the <see cref="firstUpdate"/> equal to true.
        /// This methods can be override.
        /// </remarks>
        public virtual void Update(GameTime gameTime)
        {
            GUI.Update(gameTime);

            foreach (IActor actor in listActors)
            {
                actor.Update(gameTime);
            }

            firstUpdate = true;
        }

        /// <summary>
        /// Draws all the content in the <see cref="listActors"/>
        /// only if the firstUpdate was done.
        /// </summary>
        /// <param name="gameTime">The game time.</param>
        /// <remarks>
        /// If the <see cref="camera"/> isn't null we use 
        /// <c>mainGame.spriteBatch.Begin(transformMatrix: camera.Transform);</c>
        /// to move the drawable component to camera position, all drawable
        /// outside the camera zone doesn't appear on the screen.</remarks>
        public virtual void Draw(GameTime gameTime)
        {
            if (firstUpdate)
            {
                GUI.Draw(mainGame.spriteBatch);

                if (camera != null)
                {
                    mainGame.spriteBatch.End();

                    mainGame.spriteBatch.Begin(transformMatrix: camera.Transform);
                }

                foreach (IActor actor in listActors)
                {
                    actor.Draw(mainGame.spriteBatch);
                }
            }
        }
    }
}
