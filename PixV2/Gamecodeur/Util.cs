﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gamecodeur
{
    public class Util
    {
        static Random RandomGen = new Random();

        public static void SetRandomSeed(int pSeed)
        {
            RandomGen = new Random(pSeed);
        }

        public static int GetInt(int pMin,int pMax)
        {
            return RandomGen.Next(pMin, pMax + 1);
        }

        public static bool CollideByBox(IActor p1,IActor p2)
        {
           /* if (!p1.CollideActive || !p2.CollideActive)
                return false;*/
            return p1.BoundingBox.Intersects(p2.BoundingBox);
        }

        public static Vector2 SetPositionFromBottom(Rectangle pRect,float pNewBottomY)
        {
            Vector2 position = new Vector2(pRect.X, pRect.Y)
            {
                Y = pNewBottomY - pRect.Height
            };

            return position;
        }

        public static Vector2 SetPositionFromRight(Rectangle pRect, float pNewRightX)
        {
            Vector2 position = new Vector2(pRect.X, pRect.Y)
            {
                X = pNewRightX - pRect.Width
            };

            return position;
        }
    }
}
