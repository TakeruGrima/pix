﻿using Pix;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gamecodeur
{
    /// <summary>
    /// GameState is the class who manage the scene switch
    /// </summary>
    public class GameState
    {
        /// <summary>
        /// This enum hold type scenes : Title, Menu, Gameplay and Gameover
        /// </summary>
        public enum SceneType
        {
            /// <summary>
            /// Title make reference to <see cref="SceneTitle"/>
            /// </summary>
            Title,
            /// <summary>
            /// Menu make reference to <see cref="SceneMenu"/>
            /// </summary>
            Menu,
            /// <summary>
            /// Gameplay make reference to <see cref="SceneGameplay"/>
            /// </summary>
            Gameplay,
            /// <summary>
            /// Gameover make reference to <see cref="SceneGameover"/>
            /// </summary>
            Gameover,
        }

        /// <summary>
        /// The main game
        /// </summary>
        protected MainGame mainGame;

        /// <summary>
        /// Gets or sets the current <see cref="Scene"/>.
        /// </summary>
        /// <value>
        /// The current scene.
        /// </value>
        public Scene CurrentScene { get; set; }

        /// <summary>
        /// Gets or sets the needed <see cref="IActor"/> for the next scene
        /// </summary>
        /// <value>
        /// The needed actors.
        /// </value>
        public List<IActor> NeededActors { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="GameState"/> class.
        /// </summary>
        /// <param name="pGame">The p game.</param>
        public GameState(MainGame pGame)
        {
            mainGame = pGame;
        }

        /// <summary>
        /// Switch to the <see cref="Scene"/> specified in parameter.
        /// </summary>
        /// <param name="pSceneType">Type of the p scene.</param>
        /// <remarks>
        /// Before change scene we unload all actors of the previous scene
        /// if it exist. We instantiate the currentScene and load it, see the code 
        /// bellow 
        /// <code>
        /// switch (pSceneType)
        /// {
        ///     case SceneType.Title:
        ///         CurrentScene = new SceneTitle(mainGame);
        ///     break;
        ///     case SceneType.Menu:
        ///         CurrentScene = new SceneMenu(mainGame);
        ///     break;
        ///     case SceneType.Gameplay:
        ///         CurrentScene = new SceneGameplay(mainGame);
        ///     break;
        ///     case SceneType.Gameover:
        ///         CurrentScene = new SceneGameover(mainGame);
        ///     break;
        ///     default:
        ///     break;
        /// }
        /// 
        /// CurrentScene.Load();
        /// </code>
        /// </remarks>
        public void ChangeScene(SceneType pSceneType)
        {
            if(CurrentScene!=null)
            {
                CurrentScene.UnLoad();
                CurrentScene = null;
            }

            switch (pSceneType)
            {
                case SceneType.Title:
                    CurrentScene = new SceneTitle(mainGame);
                    break;
                case SceneType.Menu:
                    CurrentScene = new SceneMenu(mainGame);
                    break;
                case SceneType.Gameplay:
                    CurrentScene = new SceneGameplay(mainGame);
                    break;
                case SceneType.Gameover:
                    CurrentScene = new SceneGameover(mainGame);
                    break;
                default:
                    break;
            }

            CurrentScene.Load();
        }
    }
}
