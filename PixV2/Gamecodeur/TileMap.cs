﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using TiledSharp;
using Microsoft.Xna.Framework.Content;

namespace Gamecodeur
{
    class TileMap : IActor
    {
        //IActor
        public bool CollisionActive { get; set; } = true;
        public Vector2 Position{ get; set; }
        public Rectangle BoundingBox { get; set; }
   
        public bool ToRemove { get; set; }
        public string Name { get; set; }
        public bool CollideActive { get; set; }

        //TileMap
        TmxMap map;
        Texture2D tileset;
        Texture2D background;

        int tileWidth;
        int tileHeight;
        int mapWidth;
        int mapHeight;
        int tilesetLines;
        int tilesetColumns;


        public TileMap(ContentManager content,string pTmxMapPath)
        {
            map = new TmxMap(pTmxMapPath);

            tileset =
            content.Load<Texture2D>(map.Tilesets[0].Name.ToString());

            if(map.ImageLayers[0] != null)
            {
                background = content.Load<Texture2D>(map.ImageLayers[0].Name.ToString());
            }

            tileWidth = map.Tilesets[0].TileWidth;
            tileHeight = map.Tilesets[0].TileHeight;
            mapWidth = map.Width;
            mapHeight = map.Height;
            tilesetColumns = tileset.Width / tileWidth;
            tilesetLines = tileset.Height / tileHeight;
        }

        public void Draw(MySpriteBatch pSpriteBatch)
        {
            if (background != null)
            {
                pSpriteBatch.Draw(background, Position, Color.White);
            }

            int nbLayers = map.Layers.Count;

            int line;
            int column;

            for (int nLayer = 0; nLayer < nbLayers; nLayer++)
            {
                line = 0;
                column = 0;

                for (int i = 0; i < map.Layers[nLayer].Tiles.Count; i++)
                {
                    int gid = map.Layers[nLayer].Tiles[i].Gid;

                    if (gid != 0)
                    {
                        int tileFrame = gid - 1;
                        int tilesetColumn = tileFrame % tilesetColumns;
                        int tilesetLine = (int)Math.Floor((double)tileFrame / (double)tilesetColumns);

                        float x = column * tileWidth;
                        float y = line * tileHeight;

                        Rectangle tilesetRec = new Rectangle(tileWidth * tilesetColumn, tileHeight * tilesetLine, tileWidth, tileHeight);

                        Vector2 tilePosition = new Vector2(x + Position.X, y + Position.Y);

                        pSpriteBatch.Draw(tileset,tilePosition, tilesetRec, Color.White);
                    }
                    column++;
                    if (column == mapWidth)
                    {
                        column = 0;
                        line++;
                    }
                }
            }
        }

        public void TouchBy(IActor pBy)
        {

        }

        public void Update(GameTime pGameTime)
        {
            
        }
    }
}
