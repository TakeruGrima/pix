﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gamecodeur
{
    public class Animation
    {
        public string Name { get; private set; }
        public int[] Frames { get; private set; }
        public float DureeFrame { get; private set; }
        public bool IsLoop { get; set; }
        public bool IsFinished { get; set; }

        public Animation(string pName, int[] pFrames, bool pisLoop = true)
        {
            Name = pName;
            Frames = pFrames;
            IsLoop = pisLoop;
            IsFinished = false;
        }
    }
    public class SpriteAnimated : Sprite
    {
        public bool Visible { get; set; } = true;
        public float Speed { get; set; }
        public int Frame { get; protected set; }
        public int FrameCounter;
        public int SwitchFrame = 120;
        public Animation currentAnimation;

        protected SpriteEffects effect;
        protected List<Animation> animations;
        protected int largeurFrame;
        protected int hauteurFrame;

        private float time;

        public SpriteAnimated(Texture2D pTexture, int pLargeurFrame, int pHauteurFrame) : base(pTexture)
        {
            largeurFrame = pLargeurFrame;
            hauteurFrame = pHauteurFrame;

            animations = new List<Animation>();
        }

        public SpriteAnimated(int pLargeurFrame, int pHauteurFrame)
        {
            largeurFrame = pLargeurFrame;
            hauteurFrame = pHauteurFrame;

            animations = new List<Animation>();
        }

        public void AddAnimation(string pName, int[] pFrames, bool pisLoop = true)
        {
            Animation animation = new Animation(pName, pFrames, pisLoop);
            animations.Add(animation);
        }

        public void PlayAnimation(string pName)
        {
            //Debug.WriteLine("LanceAnimation({0})", pName);
            foreach (Animation element in animations)
            {
                if (element.Name == pName)
                {
                    currentAnimation = element;
                    Frame = 0;
                    currentAnimation.IsFinished = false;
                    //Debug.WriteLine("LanceAnimation, OK {0}", animationCourante.name);
                    break;
                }
            }
            Debug.Assert(currentAnimation != null, "LanceAnimation : Aucune animation trouvée");
        }

        public override void Update(GameTime pGameTime)
        {
            base.Update(pGameTime);

            // Traitement des animations image par image
            if (currentAnimation == null) return;
            //if (currentAnimation.DureeFrame == 0) return;
            if (currentAnimation.IsFinished) return;

            FrameCounter += (int)pGameTime.ElapsedGameTime.TotalMilliseconds;

            if (FrameCounter >= SwitchFrame)
            {
                FrameCounter = 0;
                //Debug.WriteLine("GCSprite/Update: Changement de frame, courante={0}", frame);
                Frame++;
                if (Frame >= currentAnimation.Frames.Count())
                {
                    if (currentAnimation.IsLoop)
                    {
                        Frame = 0;
                    }
                    else
                    {
                        Frame--;
                        currentAnimation.IsFinished = true;
                    }
                }
                time = 0;
            }
        }

        public void DrawFrameAt(MySpriteBatch spriteBatch,int pnFrame, Vector2 pPosition)
        {
            Rectangle source = new Rectangle(pnFrame * largeurFrame, 0, largeurFrame, hauteurFrame);
            Vector2 origine = new Vector2(0, 0);

            spriteBatch.Draw(Texture, pPosition, source,Color.White);
        }

        public override void Draw(MySpriteBatch spriteBatch)
        {
            if(Visible)
                DrawFrameAt(spriteBatch,currentAnimation.Frames[Frame], Position);
        }
    }
}
