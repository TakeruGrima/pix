﻿using Gamecodeur;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gamecodeur
{
    // This class replace Texture2D in this project
    public class PrimitivTexture
    {
        public int Width { get; }
        public int Height { get; }

        public List2D<char> Graph { get; private set; }//represent a Texture in a 2D array 

        public PrimitivTexture(string text)
        {
            Graph = new List2D<char>();

            using (StringReader sr = new StringReader(text))
            {
                string line;
                while ((line = sr.ReadLine()) != null)
                {
                    List<char> lineList = new List<char>();

                    for (int i = 0; i < line.Length; i++)
                    {
                        lineList.Add(line[i]);
                    }

                    Graph.Add(lineList);
                }
            }

            Height = Graph.Count;
            Width = Graph[0].Count;
        }
    }
}
