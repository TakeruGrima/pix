﻿using Gamecodeur;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework.Graphics;
using System.IO;
using System.Diagnostics;
using Microsoft.Xna.Framework;

namespace Gamecodeur
{
    class PrimitivAnimation : SpriteAnimated
    {
        private bool showBoundingBox = false;//for debug

        public bool Flip { get; set; }
        List<PrimitivTexture> textures;

        public float Alpha { get; set; } = 1; 
        
        public PrimitivAnimation(string pPath,int pLargeurFrame, int pHauteurFrame) : 
            base(pLargeurFrame, pHauteurFrame)
        {
            Width = pHauteurFrame;
            Height = pLargeurFrame;

            textures = new List<PrimitivTexture>();

            string text = "";
            using (StreamReader sr = new StreamReader(pPath))
            {
                while (!sr.EndOfStream)
                {
                    string line;
                    if ((line = sr.ReadLine()) == "$")
                    {
                        textures.Add(new PrimitivTexture(text));
                        text = "";
                    }
                    else
                    {
                        text += line + "\n";
                    }
                }
            }
        }

        public new void DrawFrameAt(MySpriteBatch spriteBatch, int pnFrame, Vector2 pPosition)
        {
            spriteBatch.Draw(textures[pnFrame], pPosition, Color.Black,Alpha,Flip);
            if (showBoundingBox)
                spriteBatch.DrawRectangle(BoundingBox, 2, false, Color.Red,1);
        }

        public override void Draw(MySpriteBatch spriteBatch)
        {
            if (Visible)
                DrawFrameAt(spriteBatch, currentAnimation.Frames[Frame], Position);
        }
    }
}
