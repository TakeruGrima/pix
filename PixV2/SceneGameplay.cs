﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using System.Diagnostics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Audio;
using Gamecodeur;
using Pix.Gameplay;
using Pix.Gameplay.Character;

namespace Pix
{
    class SceneGameplay : Scene
    {
        Rectangle zoneGame;
        Player player;
        PixTileMap tileMap;

        public SceneGameplay(MainGame pGame) : base(pGame)
        {
            Debug.WriteLine("New Scene Gameplay");

        }

        public override void OnGUI()
        {
            base.OnGUI();

            Life life = new Life("Sprites/life.txt", 9, 8, 3, Screen.Width, Screen.Height)
            {
                LocalPosition = new Vector2(0, 0),
                Name = "Life"
            };

            GUI.AddElement(life);
        }

        public override void Load()
        {
            base.Load();

            Debug.WriteLine("SceneGameplay.Load");

            Rectangle Screen = mainGame.Window.ClientBounds;

            camera = new Camera(Screen);

            tileMap = new PixTileMap("Map/Map1.tmx")
            {
                Name = "TileMap"
            };

            listActors.Add(tileMap);

            foreach (DefineCharacter c in tileMap.Characters)
            {
                Vector2 tilePos = c.TilePosition;

                switch (c.Name)
                {
                    case "player":
                        player = new Player("Sprites/player.txt", 16, 23)
                        {
                            Name = "Player",
                            Speed = 50,
                            Visible = true,
                        };
                        player.SetPositionFromTile(tilePos, tileMap.TileHeight);
                        listActors.Add(player);
                        break;
                    case "ghost":
                        Character ghost = new Character("Sprites/ghost.txt", 12, 11)
                        {
                            Name = "Ghost",
                            Speed = 50,
                            Visible = true,
                        };
                        ghost.SetPositionFromTile(tilePos, tileMap.TileHeight);
                        ghost.AddAnimation("idle", new int[] { 0 }, true);
                        ghost.AddAnimation("walk", new int[] { 1, 2 }, true);
                        ghost.PlayAnimation("walk");
                        ghost.velocity = new Vector2(ghost.Speed, 0);
                        listActors.Add(ghost);
                        break;
                    default:
                        break;
                }
            }

            zoneGame = new Rectangle(0, 0, tileMap.MapWidth, tileMap.MapHeight);
        }

        public override void UnLoad()
        {
            Debug.WriteLine("SceneGameplay.Unload");
            MediaPlayer.Stop();
            //base.UnLoad();
        }

        public override void Update(GameTime gameTime)
        {
            Input.GetState();

            Clean();

            camera.Follow(player, new Rectangle(0, 0, tileMap.MapWidth, tileMap.MapHeight));

            foreach (IActor actor in listActors)
            {
                if (actor is Character c)
                {
                    List<Tile> collideTiles = tileMap.CollideWith(c);

                    if (collideTiles.Count > 0)
                    {
                        foreach (Tile tile in collideTiles)
                        {
                            c.TouchBy(tile);
                        }
                    }
                    else
                        c.Standing = false;

                    if (c != player)
                    {
                        IAPatrolTile.IASens sens = tileMap.GetIACollide(c);
                        switch (sens)
                        {
                            case IAPatrolTile.IASens.Right:
                                c.velocity = new Vector2(c.Speed, 0);
                                c.Flip = false;
                                break;
                            case IAPatrolTile.IASens.Left:
                                c.velocity = new Vector2(-c.Speed, 0);
                                c.Flip = true;
                                break;
                            case IAPatrolTile.IASens.Up:
                                c.velocity = new Vector2(0, -c.Speed);
                                break;
                            case IAPatrolTile.IASens.Down:
                                c.velocity = new Vector2(0, c.Speed);
                                break;
                        }
                    }

                    foreach (IActor character in listActors)
                    {
                        if (character is Character c2)
                        {
                            if (c != character)
                            {
                                if (Util.CollideByBox(c, character))
                                {
                                    if (player.state == State.ALIVE)
                                    {
                                        if (c == player)
                                        {
                                            player.TouchBy(c2);
                                            player.Hurt();
                                        }
                                        else
                                        {
                                            if((c != player && c2 != player) || (c2.velocity.X > 0 && c.velocity.X < 0)
                                                || c2.velocity.X < 0 && c.velocity.X > 0)
                                            {
                                                c.TouchBy(c2);
                                                c.Flip = !c.Flip;
                                            }
                                        }
                                    }
                                    else if (c != player && c2 != player)
                                    {
                                        c.TouchBy(c2);
                                        c.Flip = !c.Flip;
                                    }
                                }
                            }
                        }
                    }
                }
            }

            if (player.Position.X < 0 && player.velocity.X < 0)
            {
                player.Position = new Vector2(0, player.Position.Y);
                player.velocity.X = 0;
            }
            else if (player.BoundingBox.Right >= zoneGame.Right)
            {
                if (player.velocity.X > 0)
                {
                    player.Position = Util.SetPositionFromRight(
                       player.BoundingBox, zoneGame.Right);
                    player.velocity.X = 0;
                }
            }


            if (GUI.Find("Life") is Life life)
                life.LivesCount = (int)player.Life;

            if (player.state == State.DEAD)
            {
                foreach (IActor actor in listActors)
                {
                    if (actor is Character c)
                    {
                        c.state = State.DEAD;
                        c.PlayAnimation("idle");
                    }
                }
                player.Alpha = 1;
                mainGame.gameState.NeededActors = listActors;
                mainGame.gameState.ChangeScene(GameState.SceneType.Gameover);
            }

            base.Update(gameTime);

            Input.ReinitializeState();
        }

        public override void Draw(GameTime gameTime)
        {
            base.Draw(gameTime);
        }
    }
}
