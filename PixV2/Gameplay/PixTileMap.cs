﻿using Gamecodeur;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TiledSharp;

namespace Pix.Gameplay
{
    //Tile invisible use to make an IA Patrol
    public class IAPatrolTile
    {
        public enum IASens { Right,Left,Up,Down,NULL}

        public Vector2 Position { get; private set; }
        public Rectangle BoundingBox { get; private set; }

        public IASens Sens { get; set; }

        public IAPatrolTile(IASens pSens,Vector2 pPosition)
        {
            Sens = pSens;
            Position = pPosition;

            BoundingBox = new Rectangle(
                (int)Position.X,
                (int)Position.Y,
                32, 32);
        }
    }

    public class DefineCharacter
    {
        public string Name { get; private set; }
        public Vector2 TilePosition { get; private set; }

        public DefineCharacter(string pName,Vector2 pTilePosition)
        {
            Name = pName;
            TilePosition = pTilePosition;
        }
    }

    class PixTileMap : IActor
    {
        DefineCharacter defineCharacter;

        //IActor
        public Vector2 Position { get; set; }
        public Rectangle BoundingBox { get; set; }

        public bool ToRemove { get; set; }

        public string Name { get; set; }
        public bool CollideActive { get; set; }

        //TileMap
        TmxMap map;

        public int TileWidth { get; private set; }
        public int TileHeight { get; private set; }
        public int MapWidth { get; private set; }
        public int MapHeight { get; private set; }

        List<Tile> tiles;

        //Character position
        public List<DefineCharacter> Characters { get; private set; }

        // IA Tile patrol
        List<IAPatrolTile> patrolTiles;

        public PixTileMap(string pTmxMapPath)
        {
            map = new TmxMap(pTmxMapPath);

            TileWidth = map.Tilesets[0].TileWidth;
            TileHeight = map.Tilesets[0].TileHeight;

            MapWidth = map.Width * TileWidth;
            MapHeight = map.Height * TileHeight;

            tiles = new List<Tile>();

            Characters = new List<DefineCharacter>();
            patrolTiles = new List<IAPatrolTile>();

            int nbLayers = map.Layers.Count;

            int line;
            int column;

            for (int nLayer = 0; nLayer < nbLayers; nLayer++)
            {
                line = 0;
                column = 0;

                for (int i = 0; i < map.Layers[nLayer].Tiles.Count; i++)
                {
                    int gid = map.Layers[nLayer].Tiles[i].Gid;

                    if (gid > 1 )
                    {
                        float x = column * TileWidth;
                        float y = line * TileHeight;

                        Vector2 tilePosition = new Vector2(x + Position.X, y + Position.Y);

                        Tile tile = null;
                        switch (gid)
                        {
                            //Tile
                            case 2:
                                tile = new Tile(Tile.Type.Plate, 16, 2);
                                break;
                            case 3:
                                tile = new Tile(Tile.Type.StairLeft, 16, 2);
                                break;
                            case 4:
                                tile = new Tile(Tile.Type.StairRight, 16, 2);
                                break;
                            case 5:
                                tile = new Tile(Tile.Type.Block, 16, 2);
                                break;
                            //IA patrol tiles
                            case 6:
                                patrolTiles.Add(new IAPatrolTile(
                                    IAPatrolTile.IASens.Up,tilePosition));
                                break;
                            case 7:
                                patrolTiles.Add(new IAPatrolTile(
                                    IAPatrolTile.IASens.Down, tilePosition));
                                break;
                            case 8:
                                patrolTiles.Add(new IAPatrolTile(
                                    IAPatrolTile.IASens.Left, tilePosition));
                                break;
                            case 9:
                                patrolTiles.Add(new IAPatrolTile(
                                    IAPatrolTile.IASens.Right, tilePosition));
                                break;

                            //Character
                            case 11:
                                Characters.Add(new DefineCharacter("player", tilePosition));
                                break;
                            case 12:
                                Characters.Add(new DefineCharacter("ghost", tilePosition));
                                break;
                            default:
                                break;
                        }


                        if(gid < 6 )
                        {
                            tile.Position = tilePosition;
                            tile.UpdateBoundingBox();
                            tiles.Add(tile);
                        }
                    }
                    column++;
                    if (column == map.Width)
                    {
                        column = 0;
                        line++;
                    }
                }
            }
        }

        public void Draw(MySpriteBatch pSpriteBatch)
        {
            foreach (Tile tile in tiles)
            {
                tile.Draw(pSpriteBatch);
            }
        }

        public void RemoveTileColumn(int pX)
        {
            foreach (Tile tile in tiles)
            {
                if (tile.Position.X == pX)
                {
                    tile.ToRemove = true;
                    Debug.WriteLine("REMOVEIT");
                }
            }

            tiles.RemoveAll(item => item.ToRemove == true);
        }

        public List<Tile> CollideWith(IActor actor)
        {
            List<Tile> collideTiles = new List<Tile>();

            foreach (Tile tile in tiles)
            {
                if (Util.CollideByBox(tile, actor))
                {
                    collideTiles.Add(tile);
                }
            }
            return collideTiles;
        }

        public IAPatrolTile.IASens GetIACollide(IActor actor)
        {
            foreach (IAPatrolTile ia in patrolTiles)
            {
                if(ia.BoundingBox.Intersects(actor.BoundingBox))
                {
                    return ia.Sens;
                }
            }

            return IAPatrolTile.IASens.NULL;
        }

        public void TouchBy(IActor pBy)
        {
            
        }

        public void Update(GameTime pGameTime)
        {
            
        }
    }
}
