﻿using Gamecodeur;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pix.Gameplay
{
    class Life : GUIPanel
    {
        public int LivesCount { get; set; }

        PrimitivAnimation primitivAnimation;

        public Life(string pFileLife,int pLargeurFrame,int pHauteurFrame,int pLivesCount,
            int pPanelWidth, int pPanelHeight) : base(pPanelWidth,pPanelHeight)
        {
            LivesCount = pLivesCount;

            primitivAnimation = new PrimitivAnimation(pFileLife, pLargeurFrame, pHauteurFrame)
            {
                Position = Position
            };
            primitivAnimation.AddAnimation("idle", new int[] { 0 }, true);
            primitivAnimation.PlayAnimation("idle");
        }

        public override void TouchBy(IActor pBy)
        {

        }

        public override void Update(GameTime pGameTime)
        {
            primitivAnimation.Update(pGameTime);

            base.Update(pGameTime);
        }

        public override void Draw(MySpriteBatch pSpriteBatch)
        {
            base.Draw(pSpriteBatch);

            for (int i=0;i<LivesCount;i++)
            {
                primitivAnimation.Draw(pSpriteBatch);
                primitivAnimation.Move(primitivAnimation.Width * 2 + 4, 0);
            }

            primitivAnimation.Position = Position;
        }
    }
}
