﻿using Gamecodeur;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pix.Gameplay.Character
{
    class Player : Character
    {
        int countJump = 0;
        int accel = 500;
        int maxSpeed = 150;
        int jumpVelocity = -230;
        int hurtJumpVelocity = -150;
        int friction = 280;

        float invinsible = 2;
        float countinvisible = 0;

        public float Life { get; private set; } = 3;

        public Player(string pPath, int pLargeurFrame, int pHauteurFrame) : 
            base(pPath, pLargeurFrame, pHauteurFrame)
        {
            AddAnimation("idle", new int[] { 0 }, true);
            AddAnimation("walk", new int[] { 2, 3, 4, 5 }, true);
            AddAnimation("jump", new int[] { 1 }, true);
            PlayAnimation("idle");
        }

        public override void Update(GameTime pGameTime)
        {
            if (state == State.DEAD)
            {
                base.Update(pGameTime);
                return;
            }

            if (countinvisible > 0)
            {
                countinvisible -= (float)(pGameTime.ElapsedGameTime.TotalSeconds);
            }

            if (countinvisible <= 0)
            {
                state = State.ALIVE;
                Alpha = 1;
            }

            if (velocity.X > 0)
            {
                velocity.X = velocity.X - friction * (float)pGameTime.ElapsedGameTime.TotalSeconds;
                if (velocity.X < 0)
                {
                    velocity.X = 0;
                }
            }
            else if (velocity.X < 0)
            {
                velocity.X = velocity.X + friction * (float)pGameTime.ElapsedGameTime.TotalSeconds;
                if (velocity.X > 0)
                {
                    velocity.X = 0;
                }
            }

            if (Input.GetKey(Keys.Right))
            {
                velocity.X +=
                    accel * (float)pGameTime.ElapsedGameTime.TotalSeconds;
                if (velocity.X > maxSpeed)
                {
                    velocity.X = maxSpeed;
                }
                if (currentAnimation.Name != "walk")
                {
                    PlayAnimation("walk");
                }
                Flip = false;
            }
            else if (Input.GetKey(Keys.Left))
            {
                velocity.X -=
                    accel * (float)pGameTime.ElapsedGameTime.TotalSeconds;
                if (velocity.X < -maxSpeed)
                {
                    velocity.X = -maxSpeed;
                }
                if (currentAnimation.Name != "walk")
                {
                    PlayAnimation("walk");
                }
                Flip = true;
            }
            else
            {
                PlayAnimation("idle");
            }

            if (Input.GetKeyDown(Keys.Up) && countJump < 2)
            {
                velocity.Y += jumpVelocity;
                if (countJump == 1)
                {
                    velocity.Y -= jumpVelocity / 3;
                }
                Standing = false;
                bJumpReady = false;
                if (currentAnimation.Name != "jump")
                {
                    PlayAnimation("jump");
                }

                countJump++;
            }
            if (!Standing)
            {
                if (currentAnimation.Name != "jump")
                {
                    PlayAnimation("jump");
                }
            }

            if (Standing)
                countJump = 0;

            base.Update(pGameTime);
        }

        protected override void CollideWithCharacter(Character pCharacter)
        {
            if (pCharacter.BoundingBox.Bottom > BoundingBox.Top + 5)
            {
                if (velocity.X > 0)
                {
                    if (Standing)
                    {
                        velocity.X = -velocity.X;
                    }
                    Move(-1, 0);
                }
                else if (velocity.X < 0)
                {
                    if (Standing)
                    {
                        velocity.X = -velocity.X;
                    }
                    Move(1, 0);
                }
                if (velocity.Y > 0)
                {
                    Move(0, -1);
                    velocity.Y = hurtJumpVelocity;
                }
                else if (velocity.Y < 0)
                {
                    Move(0, 1);
                    velocity.Y = -hurtJumpVelocity;
                }
            }
        }

        public void Hurt()
        {
            if (state == State.ALIVE)
            {
                if (countinvisible <= 0)
                {
                    countinvisible = invinsible;
                    state = State.HURT;
                    Alpha = 0.5f;
                    Life -= 1;

                    if (Life == 0)
                        state = State.DEAD;
                }
            }
        }
    }
}
