﻿using Gamecodeur;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pix.Gameplay.Character
{
    public enum State { ALIVE, DEAD, HURT };

    class Character : PrimitivAnimation
    {
        public State state = State.ALIVE;

        public Vector2 velocity;

        public bool Standing { get; set; }
        protected bool bJumpReady = true;

        public Character(string pPath, int pLargeurFrame, int pHauteurFrame) :
            base(pPath, pLargeurFrame, pHauteurFrame)
        {
            Width = pLargeurFrame * 2;
            Height = pHauteurFrame * 2;
        }

        public override void Update(GameTime pGameTime)
        {
            //Sprite falling
            if (!Standing)
            {
                velocity.Y += 500 * (float)pGameTime.ElapsedGameTime.TotalSeconds;
            }

            vx = velocity.X * (float)pGameTime.ElapsedGameTime.TotalSeconds;
            vy = velocity.Y * (float)pGameTime.ElapsedGameTime.TotalSeconds;

            base.Update(pGameTime);
        }

        protected void CollideWithWorld(Tile pTile)
        {
            if (velocity.Y < 0 && pTile.Position.Y - 2 < Position.Y)
            {
                Standing = false;
                Debug.WriteLine("UP");
                velocity.Y = 0;

                if (Position.Y != pTile.Position.Y + 2)
                {
                    Position = new Vector2(Position.X, pTile.Position.Y + 2);
                }
            }
            else if (velocity.Y > 0 && pTile.Position.Y >= BoundingBox.Bottom - pTile.Size / 4)
            {
                Debug.WriteLine("DOWN");
                velocity.Y = 0;
                Standing = true;

                if (BoundingBox.Bottom != pTile.Position.Y + 2)
                {
                    Position =
                        Util.SetPositionFromBottom(BoundingBox, pTile.Position.Y + 2);
                }
            }

            UpdateBoundingBox();

            if (BoundingBox.Bottom != pTile.Position.Y + 2 &&
                BoundingBox.Top < pTile.Position.Y)
            {
                if (velocity.X > 0 && pTile.Position.X > Position.X)
                {
                    Debug.WriteLine("RIGHT");
                    velocity.X = 0;

                    if (BoundingBox.Right != pTile.Position.X)
                    {
                        Position =
                            Util.SetPositionFromRight(BoundingBox, pTile.Position.X + 1);
                    }
                }
                else if (velocity.X < 0 && pTile.Position.X < Position.X)
                {
                    Debug.WriteLine("LEFT");
                    velocity.X = 0;

                    if (Position.X != pTile.BoundingBox.Right)
                        Position = new Vector2(pTile.BoundingBox.Right - 1, Position.Y);
                }
                UpdateBoundingBox();
            }
        }

        protected virtual void CollideWithCharacter(Character pCharacter)
        {
            if (pCharacter.BoundingBox.Bottom > BoundingBox.Top + 5)
            {
                if (velocity.X > 0)
                {
                    if (Standing)
                    {
                        velocity.X = -velocity.X;
                    }
                    Move(-1, 0);
                }
                else if (velocity.X < 0)
                {
                    if (Standing)
                    {
                        velocity.X = -velocity.X;
                    }
                    Move(1, 0);
                }
            }
        }

        public override void TouchBy(IActor pBy)
        {
            if (pBy is Tile tile)
            {
                CollideWithWorld(tile);
            }
            if (pBy is Character c)
            {
                CollideWithCharacter(c);
            }
        }

        public void SetPositionFromTile(Vector2 pTilePosition, int pTileHeight)
        {
            pTilePosition.Y += pTileHeight;
            pTilePosition.Y -= (Height - 2);

            Position = pTilePosition;
        }
    }
}
