﻿using Gamecodeur;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;

namespace Pix.Gameplay
{
    class Tile : IActor
    {
        private bool showBoundingBox = false;//for debug

        public enum Type
        {
            Plate,
            StairLeft,
            StairRight,
            Block
        }

        public Vector2 Position { get; set; }
        public Rectangle BoundingBox { get; set; }

        public string Name { get; set; }
        public bool CollideActive { get; set; }

        public int I
        {
            get
            {
                return (int)Position.X / (size * scale);
            }
        }

        public int J
        {
            get
            {
                return (int)Position.Y / (size * scale);
            }
        }

        public bool ToRemove { get; set; }

        public Type TileType { get; private set; }

        private int size;
        private int scale;

        public int Size
        {
            get
            {
                return size * scale;
            }
        }

        public Tile(Type pType,int pSize,int pScale)
        {
            TileType = pType;
            size = pSize;
            scale = pScale;
        }

        public void Update(GameTime pGameTime)
        {
           
        }

        public void Draw(MySpriteBatch pSpriteBatch)
        {
            switch (TileType)
            {
                case Type.Plate:
                    pSpriteBatch.DrawLine(Position, MySpriteBatch.Direction.Horizontal, 
                        scale, size, Color.White);
                    break;
                case Type.StairLeft:
                    pSpriteBatch.DrawLine(Position, MySpriteBatch.Direction.Horizontal,
                        scale, size, Color.White);
                    pSpriteBatch.DrawLine(Position, MySpriteBatch.Direction.Vertical,
                        scale, size + 1, Color.White);
                    break;
                case Type.StairRight:
                    pSpriteBatch.DrawLine(Position, MySpriteBatch.Direction.Horizontal,
                        scale, size, Color.White);
                    pSpriteBatch.DrawLine((int)Position.X + size * scale,(int)Position.Y, 
                        MySpriteBatch.Direction.Vertical,scale, size + 1, Color.White);
                    break;
                case Type.Block:
                    pSpriteBatch.DrawLine(Position, MySpriteBatch.Direction.Horizontal,
                       scale, size, Color.White);
                    pSpriteBatch.DrawLine(Position, MySpriteBatch.Direction.Vertical,
                        scale, size + 1, Color.White);
                    pSpriteBatch.DrawLine((int)Position.X + size * scale, (int)Position.Y,
                        MySpriteBatch.Direction.Vertical, scale, size + 1, Color.White);
                    break;
            }
            if (showBoundingBox)
                pSpriteBatch.DrawRectangle(BoundingBox, 1, false, Color.Red,1);
        }

        public void TouchBy(IActor pBy)
        {

        }

        public void UpdateBoundingBox()
        {
            int x = (int)Position.X;
            int y = (int)Position.Y;
            switch (TileType)
            {
                case Type.Plate:
                    BoundingBox = new Rectangle(x,y, 
                        size * scale,2);
                    break;
                case Type.StairLeft:
                    BoundingBox = new Rectangle(x, y, 
                        size * scale, size * scale);
                    break;
                case Type.StairRight:
                    BoundingBox = new Rectangle(x, y,
                        size * scale, size * scale);
                    break;
                case Type.Block:
                    BoundingBox = new Rectangle(x, y,
                        size * scale, size * scale);
                    break;
            }
        }
    }
}
