﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using System.Diagnostics;
using Microsoft.Xna.Framework.Media;
using Gamecodeur;
using Pix.Gameplay.Character;
using Pix.Gameplay;
using Microsoft.Xna.Framework.Graphics;

namespace Pix
{
    class SceneGameover : Gamecodeur.Scene
    {
        //private Song music;
        PixTileMap tileMap = null;
        float  clearScreenX = -1;
        float toClearX;

        public SceneGameover(MainGame pGame) : base(pGame)
        {
            Debug.WriteLine("New Scene Gameover");
        }

        public override void OnGUI()
        {
            base.OnGUI();

            GUIText gameOver = new GUIText(
               mainGame.Content.Load<SpriteFont>("Fonts/GameOver"),
               "GAME OVER", Screen.Width, Screen.Height)
            {
                Name = "Game Over",
                LocalPosition = new Vector2(0, 0),
                hAlign = GUIPanel.Align.Center,
                vAlign = GUIPanel.Align.Center
            };

            GUI.AddElement(gameOver);
        }

        public override void Load()
        {
            Debug.WriteLine("SceneGameover.Load");

            listActors = mainGame.gameState.NeededActors;

            /* music = mainGame.Content.Load<Song>("cool");
             MediaPlayer.Play(music);
             MediaPlayer.IsRepeating = true;*/

            base.Load();
            camera = new Camera(Screen);
        }

        public override void UnLoad()
        {
            Debug.WriteLine("SceneGameover.Unload");
            //MediaPlayer.Stop();
            base.UnLoad();
        }

        public override void Update(GameTime gameTime)
        {
            foreach (IActor actor in listActors)
            {
                if (actor is PixTileMap)
                    tileMap = actor as PixTileMap;
                if (actor is Player player && tileMap != null)
                {
                    camera.Follow(player, new Rectangle(0, 0, tileMap.MapWidth, tileMap.MapHeight));
                }

                if (actor is Character c)
                {
                    if(c.Standing)
                        c.velocity = Vector2.Zero;

                    List<Tile> collideTiles = tileMap.CollideWith(c);

                    if (collideTiles.Count > 0)
                    {
                        foreach (Tile tile in collideTiles)
                        {
                            c.TouchBy(tile);
                        }
                    }
                    else
                        c.Standing = false;
                }
            }

            if (clearScreenX == -1)
            {
                toClearX = -camera.Translation.X + Screen.Width / 2 + tileMap.TileWidth * 2;

                if (toClearX > tileMap.MapWidth)
                {
                    toClearX = tileMap.MapWidth;
                }
                clearScreenX = toClearX - Screen.Width - +tileMap.TileWidth * 2;
            }

            Debug.WriteLine(toClearX);
            Debug.WriteLine(clearScreenX);

            base.Update(gameTime);
        }

        public override void Draw(GameTime gameTime)
        {
            if (!firstUpdate)
                return;

            mainGame.spriteBatch.End();

            mainGame.spriteBatch.Begin(transformMatrix: camera.Transform);

            tileMap.Draw(mainGame.spriteBatch);
            if (clearScreenX <= toClearX)
            {
                float oldClearScreenX = clearScreenX;

                Rectangle r = new Rectangle(0, 0, (int)clearScreenX, Screen.Height);
                mainGame.spriteBatch.DrawRectangle(r, 1, true, Color.Black, 1);

                clearScreenX += (2 * 60.0f) * (float)gameTime.ElapsedGameTime.TotalSeconds;

                int xToClear = (int)(clearScreenX / tileMap.TileWidth) * tileMap.TileWidth;
                if (clearScreenX >= xToClear)
                {
                    tileMap.RemoveTileColumn(xToClear - tileMap.TileWidth);
                }
            }
            foreach (IActor actor in listActors)
            {
                if (actor is Character c)
                    c.Draw(mainGame.spriteBatch);
            }

            mainGame.spriteBatch.End();

            mainGame.spriteBatch.Begin();

            GUI.Draw(mainGame.spriteBatch);

            if (clearScreenX >= toClearX)
                mainGame.gameState.ChangeScene(GameState.SceneType.Title);
        }
    }
}
