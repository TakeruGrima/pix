﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using System.Diagnostics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Media;
using Pix;
using System.IO;
using Gamecodeur;
using System.Windows.Forms;

namespace Pix
{
    class SceneMenu : Scene
    {
        //variable for fadeEffect
        float FadeSpeed = 1;
        bool Increase;

        GUINavigation menu;

        string text = string.Empty;

        public SceneMenu(MainGame pGame) : base(pGame)
        {
            Debug.WriteLine("New Scene Menu");
        }

        public void OnClickPlay(GUIButton pSender)
        {
            mainGame.gameState.ChangeScene(GameState.SceneType.Gameplay);
        }

        public void OnClickQuit(GUIButton pSender)
        {
            UnLoad();
            mainGame.Exit();
        }

        public override void OnGUI()
        {
            base.OnGUI();

            GUIPanel panel = new GUIPanel(Screen.Width / 2, Screen.Height / 2)
            {
                Position = new Vector2(0, 0),
                hAlign = GUIPanel.Align.Center,
                vAlign = GUIPanel.Align.Center,
                Visible = true
            };

            GUIButton play = new GUIButton(
                mainGame.Content.Load<SpriteFont>("Fonts/menuFont"), "Play", Screen.Width / 2, 0)
            {
                LocalPosition = new Vector2(0, 0),
                hAlign = GUIPanel.Align.Center,
                clickable = false
            };
            play.UpdateBoundingBox();
            play.FitText = true;
            play.OnClick = OnClickPlay;
            
            GUIButton options = new GUIButton(
                mainGame.Content.Load<SpriteFont>("Fonts/menuFont"), "Options", Screen.Width / 2, 0)
            {
                LocalPosition = new Vector2(0, play.BoundingBox.Height),
                hAlign = GUIPanel.Align.Center,
                clickable = false
            };
            options.UpdateBoundingBox();
            options.FitText = true;

            GUIButton quit = new GUIButton(
                mainGame.Content.Load<SpriteFont>("Fonts/menuFont"), "Quit", Screen.Width / 2, 0)
            {
                LocalPosition = new Vector2(0, options.BoundingBox.Height * 2),
                hAlign = GUIPanel.Align.Center,
                clickable = false
            };
            quit.FitText = true;
            quit.OnClick = OnClickQuit;
            
            menu = new GUINavigation(Screen.Width / 2, options.BoundingBox.Height * 3)
            {
                Position = new Vector2(0, 0),
                hAlign = GUIPanel.Align.Center,
                vAlign = GUIPanel.Align.Center,
            };

            GUI.AddElement(panel);
            panel.AddElement(menu);
            menu.AddElement(play);
            menu.AddElement(options);
            menu.AddElement(quit);
        }

        public override void Load()
        {
            base.Load();

            Debug.WriteLine("SceneMenu.Load");
        }

        public override void UnLoad()
        {
            Debug.WriteLine("SceneMenu.Unload");
            base.UnLoad();
        }

        public override void Update(GameTime gameTime)
        {
            Input.GetState();

            base.Update(gameTime);

            menu.ApplyFadingEffect(gameTime,FadeSpeed,ref Increase);

            Input.ReinitializeState();
        }

        public override void Draw(GameTime gameTime)
        {
            mainGame.spriteBatch.DrawString(AssetManager.MainFont, text, Vector2.Zero, Color.White);

            base.Draw(gameTime);
        }
    }
}
