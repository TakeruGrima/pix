﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using System.Diagnostics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Media;
using Pix;
using System.IO;
using Gamecodeur;

namespace Pix
{
    class PlayerCinematic : PrimitivAnimation
    {
        public int MaxPosX { get; set; }

        public PlayerCinematic(string pPath, int pLargeurFrame, int pHauteurFrame) :
            base(pPath, pLargeurFrame, pHauteurFrame)
        {

        }

        public override void Update(GameTime pGameTime)
        {
            vx = 0;
            if (Visible && Position.X < MaxPosX)
            {
                vx = Speed * (float)pGameTime.ElapsedGameTime.TotalSeconds;
            }
            base.Update(pGameTime);
        }
    }

    class LineProgress : Sprite
    {
        public int progressValue = 1;
        public int maxWidth;

        public bool Active { get; set; } = false;

        public LineProgress(Color pColor)
        {
            Height = 2;
            Width = 0;
        }

        public override void Update(GameTime pGameTime)
        {
            if (Width < maxWidth && Active)
            {
                Width += (int)((progressValue * 60.0f) *(float)pGameTime.ElapsedGameTime.TotalSeconds) ;
            }
            base.Update(pGameTime);
        }

        public override void Draw(MySpriteBatch pSpriteBatch)
        {
            pSpriteBatch.DrawLine((int)Position.X, (int)Position.Y, MySpriteBatch.Direction.Horizontal, Height, Width, Color.White);
        }
    }

    class SceneTitle : Scene
    {
        //variable for fadeEffect
        float FadeSpeed = 0.1f;
        float fadeSpeedStart = 1;
        float alpha = 0;
        float alphaPress = 0;//alpha for the pressStart
        bool Increase;

        public SceneTitle(MainGame pGame) : base(pGame)
        {
            Debug.WriteLine("New Scene Menu");
        }

        public override void OnGUI()
        {
            base.OnGUI();

            GUIText title = new GUIText(
               mainGame.Content.Load<SpriteFont>("Fonts/Title"),
               "PIX", Screen.Width, Screen.Height / 2)
            {
                Name = "Title",
                LocalPosition = new Vector2(0, 0),
                hAlign = GUIPanel.Align.Center,
                vAlign = GUIPanel.Align.Center
            };

            GUIText pressStart = new GUIText(
                mainGame.Content.Load<SpriteFont>("Fonts/PressStart"),
                "Press Start", Screen.Width, Screen.Height / 2)
            {
                Name = "Press Start",
                LocalPosition = new Vector2(0, Screen.Height / 2),
                hAlign = GUIPanel.Align.Center,
                vAlign = GUIPanel.Align.Center
            };

            GUI.AddElement(title);
            GUI.AddElement(pressStart);
        }

        public override void Load()
        {
            base.Load();

            Debug.WriteLine("SceneMenu.Load");

            // Setup Animation

            LineProgress lineProgress = new LineProgress(Color.White)
            {
                Position = new Vector2(0, Screen.Height / 2 + 64),
                maxWidth = Screen.Width
            };

            PlayerCinematic player = new PlayerCinematic("Sprites/player.txt", 16, 23)
            {
                Position = new Vector2(0, lineProgress.Position.Y - 44),
                Speed = 50,
                Visible = false,
                MaxPosX = Screen.Width / 2 - 16
            };
            player.AddAnimation("idle", new int[] { 0 }, true);
            player.AddAnimation("walk", new int[] { 2, 3, 4, 5 }, true);
            player.PlayAnimation("walk");

            listActors.Add(lineProgress);
            listActors.Add(player);

            base.Load();
        }

        public override void UnLoad()
        {
            Debug.WriteLine("SceneMenu.Unload");
            base.UnLoad();
        }

        public override void Update(GameTime gameTime)
        {
            Input.GetState();

            alpha = Gamecodeur.Effect.Fading(FadeSpeed, alpha, gameTime, Gamecodeur.Effect.FadeType.OneWay, ref Increase);

            if (alpha > 0.5f)
            {
                alphaPress = Gamecodeur.Effect.Fading(fadeSpeedStart, alphaPress, gameTime, Gamecodeur.Effect.FadeType.TwoWay, ref Increase);


                if (Input.GetKeyDown(Keys.Enter))
                {
                    mainGame.gameState.ChangeScene(GameState.SceneType.Menu);
                }
            }


            if (GUI.Find("Title") is GUIText title)
                title.Alpha = alpha;
            if (GUI.Find("Press Start") is GUIText press)
                press.Alpha = alphaPress;

            foreach (IActor actor in listActors)
            {
                if (actor is LineProgress line)
                {
                    if (alpha > 0.5f && !line.Active)
                    {
                        line.Active = true;
                    }
                }

                if (actor is PlayerCinematic pix)
                {
                    if (alpha > 0.5f)
                    {
                        pix.Visible = true;
                    }

                }
            }

            base.Update(gameTime);

            Input.ReinitializeState();
        }

        public override void Draw(GameTime gameTime)
        {
            base.Draw(gameTime);
        }
    }
}
