﻿using GrimEngine;
using GrimEngine.Util;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Pix.Gameplay;
using Pix.Scenes;

namespace Pix
{
    /// <summary>
    /// This is the main type for your game.
    /// </summary>
    public class MainGame : GrimGame
    {
        const int TargetWidth = 800;
        const int TargetHeight = 600;

        public MainGame() : base()
        {
            ScreenWidth = TargetWidth;
            ScreenHeight = TargetHeight;
            GameWidth = TargetWidth / 2;
            GameHeight = TargetHeight / 2;
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            base.Initialize();

            PlayerController.Init(1);
            PlayerController.SetMovementInput(0, Buttons.LeftThumbstickLeft, Buttons.LeftThumbstickRight, Buttons.LeftThumbstickUp, Buttons.LeftThumbstickDown);
            PlayerController.SetMovementInput(0, Keys.Left, Keys.Right, Keys.Up, Keys.Down);

            GrimRand.Init(1);

            GameState.ChangeScene<SceneGameplay>();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            base.LoadContent();
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// game-specific content.
        /// </summary>
        protected override void UnloadContent()
        {
            base.UnloadContent();

            PlayerController.Clear();
        }

        protected override void Update_Internal(GameTime gameTime)
        {
            if (Input.GetKeyDown(Keys.F))
                GrimEngineGlobals.ToggleFullScreen();

            base.Update_Internal(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            base.Draw(gameTime);
        }
    }
}
