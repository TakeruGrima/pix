﻿using GrimEngine;
using GrimEngine.Util.Graphics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pix.Gameplay
{
    public enum PixTileType
    {
        Plate = 2,
        StairLeft = 3,
        StairRight = 4,
        Block = 5
    }

    class PixTile : IActor
    {
        #region Fields

        #endregion

        #region Properties

        // IActor
        public Vector2 Position { get; set; }

        public Rectangle BoundingBox { get; protected set; }

        public bool ToRemove { get; set; }
        public bool IsActive { get; set; }

        // PixTile

        public PixTileType TileType { get; private set; }

        public int Size { get; private set; }

        #endregion

        #region Constructors

        public PixTile(PixTileType pType, int pSize)
        {
            IsActive = true;
            TileType = pType;
            Size = pSize;
        }

        #endregion

        #region Methods

        #endregion

        #region GrimEngine Methods

        public void TouchBy(IActor pBy)
        {
            
        }

        public void Update(GameTime pGameTime)
        {
            int x = (int)Position.X;
            int y = (int)Position.Y;
            switch (TileType)
            {
                case PixTileType.Plate:
                    BoundingBox = new Rectangle(x, y,
                        Size,1);
                    break;
                case PixTileType.StairLeft:
                    BoundingBox = new Rectangle(x, y,
                        Size, Size);
                    break;
                case PixTileType.StairRight:
                    BoundingBox = new Rectangle(x, y,
                        Size, Size);
                    break;
                case PixTileType.Block:
                    BoundingBox = new Rectangle(x, y,
                        Size, Size);
                    break;
            }
        }

        public void Draw(SpriteBatch pSpriteBatch)
        {
            switch (TileType)
            {
                case PixTileType.Plate:
                    pSpriteBatch.DrawLine(Position, Size, 1, Color.White, SpriteBatchExtension.eOrientation.HORIZONTAL);
                    break;
                case PixTileType.StairLeft:
                    pSpriteBatch.DrawLine(Position, Size, 1, Color.White, SpriteBatchExtension.eOrientation.HORIZONTAL);
                    pSpriteBatch.DrawLine(Position, Size + 1, 1, Color.White, SpriteBatchExtension.eOrientation.VERTICAL);
                    break;
                case PixTileType.StairRight:
                    pSpriteBatch.DrawLine(Position, Size, 1, Color.White, SpriteBatchExtension.eOrientation.HORIZONTAL);
                    pSpriteBatch.DrawLine(new Vector2(Position.X + Size, (int)Position.Y), Size + 1, 1, Color.White, SpriteBatchExtension.eOrientation.VERTICAL);
                    break;
                case PixTileType.Block:
                    pSpriteBatch.DrawLine(Position, Size, 1, Color.White, SpriteBatchExtension.eOrientation.HORIZONTAL);
                    pSpriteBatch.DrawLine(Position, Size + 1, 1, Color.White, SpriteBatchExtension.eOrientation.VERTICAL);
                    pSpriteBatch.DrawLine(new Vector2(Position.X + Size, (int)Position.Y), Size + 1, 1, Color.White, SpriteBatchExtension.eOrientation.VERTICAL);
                    break;
            }
        }

        #endregion
    }
}
