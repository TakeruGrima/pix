﻿using GrimEngine;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pix.Gameplay
{
    enum EPlayerInput
    {
        // Movement input
        LEFT, UP, RIGHT, DOWN
    }

    public enum EAxis { VERTICAL, HORIZONTAL }

    /// <summary>
    /// Class that manage input control
    /// </summary>
    class PlayerController
    {
        #region Fields

        Dictionary<EPlayerInput, Keys> mDictionaryInputKeys;
        Dictionary<EPlayerInput, Buttons> mDictionaryInputButtons;

        #endregion

        #region Properties

        public static PlayerController[] PlayerControllers { get; private set; }

        #endregion

        #region Constructors & Init

        public PlayerController()
        {
            mDictionaryInputKeys = new Dictionary<EPlayerInput, Keys>();
            mDictionaryInputButtons = new Dictionary<EPlayerInput, Buttons>();
        }

        static public void Init(int pNbPlayers)
        {
            PlayerControllers = new PlayerController[pNbPlayers];

            for (int i = 0; i < pNbPlayers; i++)
            {
                PlayerControllers[i] = new PlayerController();
            }
        }

        #endregion

        #region Methods

        static private void AddInput(int pPlayerId, EPlayerInput pPlayerInput, Keys pKey)
        {
            if (pPlayerId < 0 || pPlayerId > PlayerControllers.Length)
                return;

            PlayerController playerController = PlayerControllers[pPlayerId];

            if (playerController.mDictionaryInputKeys.ContainsKey(pPlayerInput))
                throw new Exception("Input already defined !");

            playerController.mDictionaryInputKeys.Add(pPlayerInput, pKey);
        }

        static private void AddInput(int pPlayerId, EPlayerInput pPlayerInput, Buttons pButton)
        {
            if (pPlayerId < 0 || pPlayerId > PlayerControllers.Length)
                return;

            PlayerController playerController = PlayerControllers[pPlayerId];

            if (playerController.mDictionaryInputButtons.ContainsKey(pPlayerInput))
                throw new Exception("Input already defined !");

            playerController.mDictionaryInputButtons.Add(pPlayerInput, pButton);
        }

        static public Keys GetKeyFromInput(int pPlayerId, EPlayerInput pPlayerInput)
        {
            if (pPlayerId < 0 || pPlayerId > PlayerControllers.Length)
                throw new Exception("Player id invalid!");

            if (!ExistKeyInput(pPlayerId, pPlayerInput))
                throw new Exception("Player input undefined");

            return PlayerControllers[pPlayerId].mDictionaryInputKeys[pPlayerInput];
        }

        static public Buttons GetButtonFromInput(int pPlayerId, EPlayerInput pPlayerInput)
        {
            if (pPlayerId < 0 || pPlayerId > PlayerControllers.Length)
                throw new Exception("Player id invalid!");

            if (!ExistButtonInput(pPlayerId, pPlayerInput))
                throw new Exception("Player input undefined");

            return PlayerControllers[pPlayerId].mDictionaryInputButtons[pPlayerInput];
        }

        static private bool ExistKeyInput(int pPlayerId, EPlayerInput pPlayerInput)
        {
            if (pPlayerId < 0 || pPlayerId > PlayerControllers.Length)
                throw new Exception("Player id invalid!");

            return PlayerControllers[pPlayerId].mDictionaryInputKeys.ContainsKey(pPlayerInput);
        }


        static private bool ExistButtonInput(int pPlayerId, EPlayerInput pPlayerInput)
        {
            if (pPlayerId < 0 || pPlayerId > PlayerControllers.Length)
                throw new Exception("Player id invalid!");

            return PlayerControllers[pPlayerId].mDictionaryInputButtons.ContainsKey(pPlayerInput);
        }

        static public void SetMovementInput(int pPlayerId, Keys pMoveLeft, Keys pMoveRight, Keys pMoveUp, Keys pMoveDown)
        {
            AddInput(pPlayerId, EPlayerInput.LEFT, pMoveLeft);
            AddInput(pPlayerId, EPlayerInput.RIGHT, pMoveRight);
            AddInput(pPlayerId, EPlayerInput.UP, pMoveUp);
            AddInput(pPlayerId, EPlayerInput.DOWN, pMoveDown);
        }

        static public void SetMovementInput(int pPlayerId, Buttons pMoveLeft, Buttons pMoveRight, Buttons pMoveUp, Buttons pMoveDown)
        {
            AddInput(pPlayerId, EPlayerInput.LEFT, pMoveLeft);
            AddInput(pPlayerId, EPlayerInput.RIGHT, pMoveRight);
            AddInput(pPlayerId, EPlayerInput.UP, pMoveUp);
            AddInput(pPlayerId, EPlayerInput.DOWN, pMoveDown);
        }
        static public void SetPlayerInput(int pPlayerId, EPlayerInput pInput, Keys pKey)
        {
            AddInput(pPlayerId, pInput, pKey);
        }

        static public void SetPlayerInput(int pPlayerId, EPlayerInput pInput, Buttons pButton)
        {
            AddInput(pPlayerId, pInput, pButton);
        }

        static public bool IsPlayerInput(int pPlayerId, EPlayerInput playerInput)
        {
            bool bKeyExist = ExistKeyInput(pPlayerId, playerInput);
            bool bButtonExist = ExistButtonInput(pPlayerId, playerInput);

            bool bKey = false;
            bool bButton = false;

            if (bKeyExist)
                bKey = Input.GetKey(GetKeyFromInput(pPlayerId, playerInput));
            if (bButtonExist)
                bButton = Input.GetButton(pPlayerId, GetButtonFromInput(pPlayerId, playerInput));

            return bKey || bButton;
        }

        static public bool IsPlayerInputDown(int pPlayerId, EPlayerInput playerInput)
        {
            bool bKeyExist = ExistKeyInput(pPlayerId, playerInput);
            bool bButtonExist = ExistButtonInput(pPlayerId, playerInput);

            bool bKey = false;
            bool bButton = false;

            if (bKeyExist)
                bKey = Input.GetKeyDown(GetKeyFromInput(pPlayerId, playerInput));
            if (bButtonExist)
                bButton = Input.GetButtonDown(pPlayerId, GetButtonFromInput(pPlayerId, playerInput));

            return bKey || bButton;
        }

        static public bool IsPlayerInputUp(int pPlayerId, EPlayerInput playerInput)
        {
            bool bKeyExist = ExistKeyInput(pPlayerId, playerInput);
            bool bButtonExist = ExistButtonInput(pPlayerId, playerInput);

            bool bKey = false;
            bool bButton = false;

            if (bKeyExist)
                bKey = Input.GetKeyUp(GetKeyFromInput(pPlayerId, playerInput));
            if (bButtonExist)
                bButton = Input.GetButtonUp(pPlayerId, GetButtonFromInput(pPlayerId, playerInput));

            return bKey || bButton;
        }

        static public float GetAxis(int pPlayerId, EAxis pAxis)
        {
            if (pAxis == EAxis.HORIZONTAL)
            {
                if (IsPlayerInput(pPlayerId, EPlayerInput.LEFT))
                    return -1;
                else if (IsPlayerInput(pPlayerId, EPlayerInput.RIGHT))
                    return 1;

                return 0;
            }
            else if (pAxis == EAxis.VERTICAL)
            {
                if (IsPlayerInput(pPlayerId, EPlayerInput.UP))
                    return -1;
                else if (IsPlayerInput(pPlayerId, EPlayerInput.DOWN))
                    return 1;

                return 0;
            }
            else
            {
                throw new Exception("Invalid EAxis argument!");
            }
        }

        static public void Clear()
        {
            for (int i = 0; i < PlayerControllers.Length; i++)
            {
                PlayerControllers[i].mDictionaryInputKeys.Clear();
                PlayerControllers[i].mDictionaryInputButtons.Clear();
            }
        }

        #endregion
    }
}
