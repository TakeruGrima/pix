﻿using GrimEngine;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TiledLib;
using TiledLib.Layer;

namespace Pix.Gameplay
{
    class PixMap : ITileMapActor
    {
        #region Fields

        protected Map mTiledMap;

        private List<PixTile> mTiles;

        #endregion

        #region Properties

        // ITileMapActor
        public bool IsActive { get; set; }
        public Vector2 Position { get; set; }
        public Rectangle BoundingBox { get; protected set; }
        public bool ToRemove { get; set; }

        public int TileWidth
        {
            get
            {
                if (mTiledMap != null)
                    return mTiledMap.CellWidth;

                return 0;
            }
        }
        public int TileHeight
        {
            get
            {
                if (mTiledMap != null)
                    return mTiledMap.CellHeight;

                return 0;
            }
        }
        public int Width
        {
            get
            {
                if (mTiledMap != null)
                    return mTiledMap.Width;

                return 0;
            }
        }

        public int Height
        {
            get
            {
                if (mTiledMap != null)
                    return mTiledMap.Height;

                return 0;
            }
        }

        #endregion

        #region Constructor

        public PixMap(string pMapName)
        {
            mTiledMap = AssetManager.Load<Map>(pMapName);

            IsActive = true;

            mTiles = new List<PixTile>();

            int nbLayers = mTiledMap.Layers.Length;

            int line;
            int column;

            for (int nLayer = 0; nLayer < nbLayers; nLayer++)
            {
                line = 0;
                column = 0;

                TileLayer layer = mTiledMap.Layers[nLayer] as TileLayer;

                string layerName = layer.Name;

                for (int i = 0; i < layer.Data.Length; i++)
                {
                    int gid = layer.Data[i];

                    if (layerName == "Plateform" && gid > 0)
                    {
                        float x = column * TileWidth;
                        float y = line * TileHeight;

                        Vector2 tilePosition = new Vector2(x + Position.X, y + Position.Y);

                        PixTile tile = new PixTile((PixTileType)gid,TileWidth);

                        if(tile != null)
                        {
                            tile.Position = tilePosition;
                            mTiles.Add(tile);
                        }
                    }
                    column++;
                    if (column == Width)
                    {
                        column = 0;
                        line++;
                    }
                }
            }
        }

        #endregion

        #region Methods

        // Public -------------------------------------------------------

        public virtual int GetTileAt(float x, float y, int pLayerId)
        {
            return -1;
        }

        public virtual bool IsTileSolid(int pTileId)
        {
            return false;
        }

        // Private ------------------------------------------------------

        #endregion

        #region GrimEngine Methods

        public virtual void TouchBy(IActor pBy)
        {

        }

        public virtual void Update(GameTime pGameTime)
        {
            BoundingBox = new Rectangle((int)Position.X, (int)Position.Y, Width * TileWidth, Height * TileHeight);
        }

        public virtual void Draw(SpriteBatch pSpriteBatch)
        {
            foreach (PixTile tile in mTiles)
            {
                tile.Draw(pSpriteBatch);
            }
        }

        #endregion
    }
}
