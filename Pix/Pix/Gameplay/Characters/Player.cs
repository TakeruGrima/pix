﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pix.Gameplay.Characters
{
    class Player : Character
    {
        #region Fields

        const int ACCEL = 500;
        const int MAX_SPEED = 150;
        const int FRICTION = 280;

        #endregion

        #region Constructors

        public Player(string pPath) : base(pPath)
        {
            AddAnimation("Idle", new int[] { 0 },120, true);
            AddAnimation("Walk", new int[] { 2, 3, 4, 5 },120, true);
            SetAnimation("Idle");
        }

        #endregion

        #region Methods

        #endregion

        #region GrimEngine Methods

        public override void Update(GameTime pGameTime)
        {
            if (mVelocity.X > 0)
            {
                mVelocity.X = mVelocity.X - FRICTION * (float)pGameTime.ElapsedGameTime.TotalSeconds;
                if (mVelocity.X < 0)
                {
                    mVelocity.X = 0;
                }
            }
            else if (mVelocity.X < 0)
            {
                mVelocity.X = mVelocity.X + FRICTION * (float)pGameTime.ElapsedGameTime.TotalSeconds;
                if (mVelocity.X > 0)
                {
                    mVelocity.X = 0;
                }
            }

            float moveX = ACCEL * PlayerController.GetAxis(0, EAxis.HORIZONTAL);

            mVelocity.X += moveX;

            if(Math.Abs(mVelocity.X) > MAX_SPEED)
            {
                mVelocity.X = mVelocity.X > 0 ? MAX_SPEED : -MAX_SPEED;
            }

            if (Math.Abs(moveX) > 0)
            {
                SetAnimation("Walk");
                IsFlip = mVelocity.X < 0;
            }
            else
            {
                SetAnimation("Idle");
            }

            base.Update(pGameTime);
        }

        #endregion
    }
}
