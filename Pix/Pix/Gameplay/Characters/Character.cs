﻿using GrimEngine;
using Microsoft.Xna.Framework;
using Pix.Engine.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pix.Gameplay.Characters
{
    class Character : AnimatedPrimSprite, IMovingActor
    {
        #region Fields

        protected Vector2 mVelocity;

        #endregion

        #region Properties

        public Rectangle HitBox { get; protected set; }

        #endregion

        #region Constructors

        public Character(string pPath) : base(pPath)
        {
        }

        #endregion

        #region Methods

        /// <summary>
        /// Create bounding box at position
        /// </summary>
        /// <param name="pPosition"></param>
        /// <returns></returns>
        protected virtual Rectangle CreateBoundingBox(Vector2 pPosition)
        {
            return new Rectangle((int)Math.Round(pPosition.X), (int)Math.Round(pPosition.Y), BoundingBox.Width, BoundingBox.Height);
        }

        /// <summary>
        /// Create hit box at position. Override it to have a custom hit box different than bounding box
        /// </summary>
        /// <param name="pPosition"></param>
        /// <returns></returns>
        protected virtual Rectangle CreateHitBox(Vector2 pPosition)
        {
            return BoundingBox;
        }

        protected void UpdateHitBox()
        {
            HitBox = CreateHitBox(Position);
        }

        #endregion

        #region GrimEngine Methods

        public override void UpdateBoundingBox()
        {
            base.UpdateBoundingBox();
            UpdateHitBox();
        }

        public override void Update(GameTime pGameTime)
        {
            vx = mVelocity.X * (float)pGameTime.ElapsedGameTime.TotalSeconds;
            vy = mVelocity.Y * (float)pGameTime.ElapsedGameTime.TotalSeconds;

            base.Update(pGameTime);
        }

        #endregion
    }
}
