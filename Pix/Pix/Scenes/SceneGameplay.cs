﻿using GrimEngine;
using GrimEngine.Util.Graphics;
using Microsoft.Xna.Framework;
using Pix.Gameplay;
using Pix.Gameplay.Characters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pix.Scenes
{
    class SceneGameplay : Scene
    {
        #region Fields

        #endregion

        #region Constructor

        public SceneGameplay() : base()
        {
        }

        #endregion

        #region Methods

        public override void Destroy()
        {
            base.Destroy();
        }

        #endregion

        #region GrimEngine Methods

        protected override void OnUI()
        {
            base.OnUI();
        }

        public override void Load()
        {
            PixMap map = new PixMap(@"Map1");

            World.Instance.Map = map;
            AddActor(map);

            Player player = new Player("Sprites/player.txt")
            {
                Position = new Vector2(GrimEngineGlobals.GameWidth / 2, GrimEngineGlobals.GameHeight / 2)
            };
            AddActor(player);
            World.Instance.AddPlayer(player);

            base.Load();

            Camera.Follow(player, new Rectangle(0, 0, GrimEngineGlobals.GameWidth, GrimEngineGlobals.GameHeight));
        }

        public override void UnLoad()
        {
            base.UnLoad();
        }

        protected override void Update(GameTime gameTime)
        {
            
        }

        protected override void Draw(GameTime gameTime)
        {
            base.Draw(gameTime);
        }

        #endregion
    }
}
