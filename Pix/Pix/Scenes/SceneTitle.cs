﻿using GrimEngine;
using Microsoft.Xna.Framework;
using Pix.Engine.Graphics;
using Pix.Gameplay.Characters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pix.Scenes
{
    class SceneTitle : Scene
    {
        #region Fields

        #endregion

        #region Constructor

        public SceneTitle() : base()
        {
        }

        #endregion

        #region Methods

        public override void Destroy()
        {
            base.Destroy();
        }

        #endregion

        #region GrimEngine Methods

        protected override void OnUI()
        {
            base.OnUI();
        }

        public override void Load()
        {
            base.Load();
        }

        public override void UnLoad()
        {
            base.UnLoad();
        }

        protected override void Update(GameTime gameTime)
        {
            
        }

        #endregion
    }
}
