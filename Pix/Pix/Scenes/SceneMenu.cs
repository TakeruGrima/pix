﻿using GrimEngine;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pix.Scenes
{
    class SceneMenu : Scene
    {
        #region Fields

        #endregion

        #region Constructor

        public SceneMenu() : base()
        {
        }

        #endregion

        #region Methods

        public override void Destroy()
        {
            base.Destroy();
        }

        #endregion

        #region GrimEngine Methods

        protected override void OnUI()
        {
            base.OnUI();
        }

        public override void UnLoad()
        {
            base.UnLoad();
        }

        protected override void Update(GameTime gameTime)
        {
            
        }

        #endregion
    }
}
