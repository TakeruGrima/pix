﻿using GrimEngine;
using GrimEnginePipelineExtension;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pix.Engine.Graphics
{
    class PrimitivAnimation : AAnimation
    {
        #region Fields

        List<PrimitivTexture> mListFrame;

        #endregion

        #region Properties

        public PrimitivTexture FrameTexture
        {
            get
            {
                return mListFrame[(int)CurrentFrame.X];
            }
        }

        public bool IsFlip { get; set; }

        #endregion

        #region Constructor

        public PrimitivAnimation(List<PrimitivTexture> pFrames,Vector2 pCurrentFrame,float pDuration,bool pIsLoop) : base(pCurrentFrame)
        {
            if(pFrames == null || pFrames.Count == 0)
            {
                throw new Exception("List frame is null or empty !");
            }

            mListFrame = pFrames;

            mColumnCount = pFrames.Count;
            mRowCount = 1;

            AmountOfFrames = new Vector2(mColumnCount, mRowCount);

            DureeFrame = pDuration;
            IsLoop = pIsLoop;
            IsFinished = false;

            FrameWidth = pFrames[0].Width;
            FrameHeight = pFrames[0].Height;
        }

        #endregion
    }

    class AnimatedPrimSprite : AnimatedASprite
    {
        #region Fields

        List<PrimitivTexture> mLoadedTextures;

        #endregion

        #region Properties

        public Color BackColor { get; set; }
        public PrimitivAnimation CurrentPrimitivAnimation { get; private set; }

        public bool IsFlip
        {
            get
            {
                if (CurrentPrimitivAnimation == null)
                    return false;

                return CurrentPrimitivAnimation.IsFlip;
            }
            set
            {
                CurrentPrimitivAnimation.IsFlip = value;
                CurrentAnimation = CurrentPrimitivAnimation;
            }
        }

        #endregion

        #region Constructors

        public AnimatedPrimSprite(string pPath) : base()
        {
            mLoadedTextures = new List<PrimitivTexture>();

            string text = "";
            using (StreamReader sr = new StreamReader(pPath))
            {
                while (!sr.EndOfStream)
                {
                    string line;
                    if ((line = sr.ReadLine()) == "$")
                    {
                        mLoadedTextures.Add(new PrimitivTexture(text));
                        text = "";
                    }
                    else
                    {
                        text += line + "\n";
                    }
                }
            }
        }

        #endregion

        #region Methods

        public void AddAnimation(string pName, int[] pFrames,float pDuration, bool pIsLoop = true)
        {
            List<PrimitivTexture> listFrames = new List<PrimitivTexture>();

            for (int i = 0; i < pFrames.Length; i++)
            {
                int frameId = pFrames[i];

                if (frameId < 0 || frameId > mLoadedTextures.Count)
                    throw new Exception("Wrong index in Frames array !");

                listFrames.Add(mLoadedTextures[frameId]);
            }

            PrimitivAnimation animation = new PrimitivAnimation(listFrames, Vector2.Zero, pDuration,pIsLoop);
            mAnimations.Add(pName,animation);
        }

        public override void SetAnimation(string pName)
        {
            bool bIsFlipOld = IsFlip;
            base.SetAnimation(pName);

            CurrentPrimitivAnimation = CurrentAnimation as PrimitivAnimation;
            CurrentPrimitivAnimation.IsFlip = bIsFlipOld;
        }

        #endregion

        #region GrimEngine Methods

        public override void UpdateBoundingBox()
        {
            base.UpdateBoundingBox();
        }

        public override void Draw(SpriteBatch pSpriteBatch)
        {
            if (CurrentAnimation == null) return;

            pSpriteBatch.Draw(CurrentPrimitivAnimation.FrameTexture, Position, BackColor, Alpha, IsFlip);
        }

        #endregion
    }
}
