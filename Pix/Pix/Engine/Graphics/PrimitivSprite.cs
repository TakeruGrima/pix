﻿using GrimEngine;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pix.Engine.Graphics
{
    /// <summary>
    /// This class replace Sprite in this project, it's a sprite that don't use Texture2D
    /// </summary>
    class PrimitivSprite : ASprite
    {
        #region Fields

        PrimitivTexture mTexture;

        #endregion

        #region Properties

        public Color BackColor { get; set; }
        public bool IsFlip { get; set; }

        #endregion

        #region Constructors

        public PrimitivSprite(string pPath)
        {
            IsFlip = false;

            string text = "";
            using (StreamReader sr = new StreamReader(pPath))
            {
                while (!sr.EndOfStream)
                {
                    string line = sr.ReadLine();
                    text += line + "\n";
                }
            }

            mTexture = new PrimitivTexture(text);
        }

        #endregion

        #region GrimEngine Methods

        public override void UpdateBoundingBox()
        {
            BoundingBox = new Rectangle((int)Position.X, (int)Position.Y, mTexture.Width, mTexture.Height);
        }

        public override void Draw(SpriteBatch pSpriteBatch)
        {
            pSpriteBatch.Draw(mTexture, Position, BackColor, Alpha,IsFlip);
        }

        #endregion
    }
}
