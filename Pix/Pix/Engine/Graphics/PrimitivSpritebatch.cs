﻿using GrimEngine.Util.Graphics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pix.Engine.Graphics
{
    public static class PrimitivSpritebatch
    {
        #region Methods

        static public void Draw(this SpriteBatch spriteBatch,PrimitivTexture pPrimTexture, Vector2 pPosition, Color pBackColor, float pAlpha, bool pFlip)
        {
            for (int j = 0; j < pPrimTexture.Height; j++)
            {
                if (pFlip == false)
                {
                    for (int i = 0; i < pPrimTexture.Width; i++)
                    {
                        if (pPrimTexture.Graph[j, i] == '1')
                        {
                            spriteBatch.DrawPoint((int)pPosition.X + i, (int)pPosition.Y + j, 1, Color.White, pAlpha);
                        }
                        else if (pPrimTexture.Graph[j, i] == '2')
                        {
                            spriteBatch.DrawPoint((int)pPosition.X + i, (int)pPosition.Y + j, 1, pBackColor, 1);
                        }
                    }
                }
                else
                {
                    for (int i = pPrimTexture.Width - 1; i >= 0; i--)
                    {
                        if (pPrimTexture.Graph[j, i] == '1')
                        {
                            spriteBatch.DrawPoint((int)pPosition.X + ((pPrimTexture.Width - 1) - i), (int)pPosition.Y + j,
                                1, Color.White, pAlpha);
                        }
                        else if (pPrimTexture.Graph[j, i] == '2')
                        {
                            spriteBatch.DrawPoint((int)pPosition.X + ((pPrimTexture.Width - 1) - i), (int)pPosition.Y + j,1, pBackColor, 1);
                        }
                    }
                }
            }
        }

        #endregion
    }
}
