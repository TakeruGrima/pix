# Pix

Plateform Game for GameJam11 of Gamecodeur A game in C# Monogame. Subject: A game without using image files.
It's a remake of the Pix game on my github, it's use an engine that I used for my games.

You can find an exe file to play the game in Application Folder.
The controls are :
 - In the title screen press Enter to play
 - In the menu press arrow key to navigate and Enter to choose
 - Left and Right arrow to move Pix
 - Up arrow to jump (press 2 times up to make a double jump)

Do not copy
